﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveHealper : MonoBehaviour 
{
 public void Save(Texture2D texture2D)
    {
        var bytes = texture2D.EncodeToPNG();

        var dirPath = Application.dataPath + "/../SaveImages/";

        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        var tileStamp = DateTime.Now.ToString("123");
        File.WriteAllBytes(dirPath + "Image-" + tileStamp + ".png", bytes);
    }
}

