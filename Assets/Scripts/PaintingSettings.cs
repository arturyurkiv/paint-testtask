﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintingSettings : MonoBehaviour
{

	private float _transparency = 1f;
	private CUIColorPicker _cUIColorPicker;

	private void Start()
	{
		_cUIColorPicker = FindObjectOfType<CUIColorPicker>();
	}

	public void SetMarkerColour(Color new_color)
	{
		Painter.PenColour = new_color;
	}
    
	public void SetMarkerWidth(int newWidth)
	{
		Painter.PenWidth = newWidth;
	}
	public void SetMarkerWidth(float newWidth)
	{
		SetMarkerWidth((int)newWidth);
	}

	public void SetNewColor()
	{
		Color color = _cUIColorPicker.Color;
		color.a = _transparency;
		SetMarkerColour(color);
		Painter._drawable.SetPenBrush();
	}

	public void SetEraser()
	{
		SetMarkerColour(new Color(1f, 1f, 1f, 1f));
	}

	public void PartialSetEraser()
	{
		SetMarkerColour(new Color(255f, 255f, 255f, 0.5f));
	}
}

