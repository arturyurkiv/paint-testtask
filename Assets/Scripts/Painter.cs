﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Painter : MonoBehaviour
{
    public static Color PenColour = Color.red;
    public static int PenWidth = 3;
    public static Painter _drawable;


    [SerializeField] private LayerMask _drawingLayers;

    private delegate void BrushFunction(Vector2 world_position);

    private BrushFunction _currentBrush;

    private Color _resetColour;
    private Color[] _cleanColoursArray;
    private Color32[] _curentColors;

    private Sprite _drawableSprite;
    private Texture2D _drawableTexture;

    private Vector2 _previousDragPosition;


    private void Awake()
    {
        
        _resetColour = Color.white;
        _drawable = this;
        _currentBrush = PenBrush;

        _drawableSprite = this.GetComponent<SpriteRenderer>().sprite;
        _drawableTexture = _drawableSprite.texture;
        ResetCanvas();

    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Collider2D hit = Physics2D.OverlapPoint(mouseWorldPosition, _drawingLayers.value);
            if (hit != null && hit.transform != null)
            {
                _currentBrush(mouseWorldPosition);
            }
            else
            {
                _previousDragPosition = Vector2.zero;
            }
        }
        else if (!Input.GetMouseButton(0))
        {
            _previousDragPosition = Vector2.zero;
        }
    }
    
    private void ColourPixels(Vector2 centerPixel, int penThickness, Color colorPen)
    {
        int centerX = (int)centerPixel.x;
        int centerY = (int)centerPixel.y;

        for (int x = centerX - penThickness; x <= centerX + penThickness; x++)
        {
            for (int y = centerY - penThickness; y <= centerY + penThickness; y++)
            {
                _drawableTexture.SetPixel(x, y, colorPen);
            }
        }

        _drawableTexture.Apply();
    }



    private void PenBrush(Vector2 worldPoint)
    {
        Vector2 pixelPosition = WorldToPixelCoordinates(worldPoint);

        _curentColors = _drawableTexture.GetPixels32();

        if (_previousDragPosition == Vector2.zero)
        {
            MarkPixelsToColour(pixelPosition, PenWidth, PenColour);
        }
        else
        {
            Interpolation(_previousDragPosition, pixelPosition, PenWidth, PenColour);
        }

        ApplyMarkedPixelChanges();
        _previousDragPosition = pixelPosition;
    }
    
    private void MarkPixelsToColour(Vector2 centerPixel, int penThickness, Color penColor)
    {
        int centerX = (int)centerPixel.x;
        int centerY = (int)centerPixel.y;

        for (int x = centerX - penThickness; x <= centerX + penThickness; x++)
        {
            if (x >= (int)_drawableSprite.rect.width || x < 0)
                continue;

            for (int y = centerY - penThickness; y <= centerY + penThickness; y++)
            {
                MarkPixelToChange(x, y, penColor);
            }
        }
    }
    private void MarkPixelToChange(int x, int y, Color color)
    {
        int arrayPosition = y * (int)_drawableSprite.rect.width + x;

        if (arrayPosition > _curentColors.Length || arrayPosition < 0)
            return;

        _curentColors[arrayPosition] = color;
    }
   
    private void ApplyMarkedPixelChanges()
    {
        _drawableTexture.SetPixels32(_curentColors);
        _drawableTexture.Apply();
    }

    
    private void Interpolation(Vector2 startPoint, Vector2 endPoint, int width, Color color)
    {
        float distance = Vector2.Distance(startPoint, endPoint);
        Vector2 direction = (startPoint - endPoint).normalized;

        Vector2 currentPosition = startPoint;
        float lerpSteps = 1 / distance;

        for (float lerp = 0; lerp <= 1; lerp += lerpSteps)
        {
            currentPosition = Vector2.Lerp(startPoint, endPoint, lerp);
            MarkPixelsToColour(currentPosition, width, color);
        }
    }

    
    private Vector2 WorldToPixelCoordinates(Vector2 worldPosition)
    {
        Vector3 localPosition = transform.InverseTransformPoint(worldPosition);
        float pixelWidth = _drawableSprite.rect.width;
        float pixelHeight = _drawableSprite.rect.height;
        float unitsToPixels = pixelWidth / _drawableSprite.bounds.size.x * transform.localScale.x;


        float centered_x = localPosition.x * unitsToPixels + pixelWidth / 2;
        float centered_y = localPosition.y * unitsToPixels + pixelHeight / 2;


        Vector2 pixelPosition = new Vector2(Mathf.RoundToInt(centered_x), Mathf.RoundToInt(centered_y));

        return pixelPosition;
    }
    
    private void ResetCanvas()
    {
        _cleanColoursArray = new Color[(int)_drawableSprite.rect.width * (int)_drawableSprite.rect.height];
        for (int x = 0; x < _cleanColoursArray.Length; x++)
            _cleanColoursArray[x] = _resetColour;

        _drawableTexture.SetPixels(_cleanColoursArray);
        _drawableTexture.Apply();
    }

    public void SetPenBrush()
    {
        _currentBrush = PenBrush;
    }

}