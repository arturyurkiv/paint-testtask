﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// CUIColorPicker
struct CUIColorPicker_t3452025155;
// CUIColorPicker/<Setup>c__AnonStorey0
struct U3CSetupU3Ec__AnonStorey0_t1509386468;
// Painter
struct Painter_t2372114961;
// Painter/BrushFunction
struct BrushFunction_t136629315;
// PaintingSettings
struct PaintingSettings_t3763250382;
// SaveHealper
struct SaveHealper_t3871815534;
// System.Action
struct Action_t1264377477;
// System.Action`1<UnityEngine.Color>
struct Action_1_t2728153919;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IO.DirectoryInfo
struct DirectoryInfo_t35957480;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Random
struct Random_t108471755;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;

extern RuntimeClass* Action_t1264377477_il2cpp_TypeInfo_var;
extern RuntimeClass* BrushFunction_t136629315_il2cpp_TypeInfo_var;
extern RuntimeClass* ColorU5BU5D_t941916413_il2cpp_TypeInfo_var;
extern RuntimeClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Painter_t2372114961_il2cpp_TypeInfo_var;
extern RuntimeClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern RuntimeClass* Random_t108471755_il2cpp_TypeInfo_var;
extern RuntimeClass* RectTransform_t3704657025_il2cpp_TypeInfo_var;
extern RuntimeClass* SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CSetupU3Ec__AnonStorey0_t1509386468_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1700643193;
extern String_t* _stringLiteral2410385622;
extern String_t* _stringLiteral2671228154;
extern String_t* _stringLiteral3504223596;
extern String_t* _stringLiteral3684129841;
extern String_t* _stringLiteral405360011;
extern String_t* _stringLiteral72992802;
extern String_t* _stringLiteral819375327;
extern String_t* _stringLiteral866954529;
extern const RuntimeMethod* Action_1_Invoke_m3561268814_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisCUIColorPicker_t3452025155_m1733115665_RuntimeMethod_var;
extern const RuntimeMethod* Painter_PenBrush_m323187877_RuntimeMethod_var;
extern const RuntimeMethod* U3CSetupU3Ec__AnonStorey0_U3CU3Em__0_m1864855913_RuntimeMethod_var;
extern const RuntimeMethod* U3CSetupU3Ec__AnonStorey0_U3CU3Em__1_m3821171049_RuntimeMethod_var;
extern const RuntimeMethod* U3CSetupU3Ec__AnonStorey0_U3CU3Em__2_m2247192937_RuntimeMethod_var;
extern const RuntimeMethod* U3CSetupU3Ec__AnonStorey0_U3CU3Em__3_m4203508073_RuntimeMethod_var;
extern const RuntimeMethod* U3CSetupU3Ec__AnonStorey0_U3CU3Em__4_m1100181865_RuntimeMethod_var;
extern const RuntimeMethod* U3CSetupU3Ec__AnonStorey0_U3CU3Em__5_m3056497001_RuntimeMethod_var;
extern const uint32_t BrushFunction_BeginInvoke_m3305966453_MetadataUsageId;
extern const uint32_t CUIColorPicker_GetLocalMouse_m3410064497_MetadataUsageId;
extern const uint32_t CUIColorPicker_GetWidgetSize_m2427733028_MetadataUsageId;
extern const uint32_t CUIColorPicker_RGBToHSV_m132274398_MetadataUsageId;
extern const uint32_t CUIColorPicker_SetRandomColor_m2343968930_MetadataUsageId;
extern const uint32_t CUIColorPicker_Setup_m1889472872_MetadataUsageId;
extern const uint32_t Painter_Awake_m3430070722_MetadataUsageId;
extern const uint32_t Painter_Interpolation_m2325092361_MetadataUsageId;
extern const uint32_t Painter_PenBrush_m323187877_MetadataUsageId;
extern const uint32_t Painter_ResetCanvas_m1980404891_MetadataUsageId;
extern const uint32_t Painter_SetPenBrush_m424290498_MetadataUsageId;
extern const uint32_t Painter_Update_m1027624347_MetadataUsageId;
extern const uint32_t Painter_WorldToPixelCoordinates_m3555177886_MetadataUsageId;
extern const uint32_t Painter__cctor_m662125785_MetadataUsageId;
extern const uint32_t PaintingSettings_SetMarkerColour_m484989563_MetadataUsageId;
extern const uint32_t PaintingSettings_SetMarkerWidth_m3501128933_MetadataUsageId;
extern const uint32_t PaintingSettings_SetNewColor_m3890990801_MetadataUsageId;
extern const uint32_t PaintingSettings_Start_m1354421605_MetadataUsageId;
extern const uint32_t SaveHealper_Save_m4095981465_MetadataUsageId;
extern const uint32_t U3CSetupU3Ec__AnonStorey0_U3CU3Em__1_m3821171049_MetadataUsageId;
extern const uint32_t U3CSetupU3Ec__AnonStorey0_U3CU3Em__2_m2247192937_MetadataUsageId;
extern const uint32_t U3CSetupU3Ec__AnonStorey0_U3CU3Em__3_m4203508073_MetadataUsageId;
extern const uint32_t U3CSetupU3Ec__AnonStorey0_U3CU3Em__4_m1100181865_MetadataUsageId;
extern const uint32_t U3CSetupU3Ec__AnonStorey0_U3CU3Em__5_m3056497001_MetadataUsageId;

struct ByteU5BU5D_t4116647657;
struct SingleU5BU5D_t1444911251;
struct Color32U5BU5D_t3850468773;
struct ColorU5BU5D_t941916413;


#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef RANDOM_T108471755_H
#define RANDOM_T108471755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Random
struct  Random_t108471755  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_0;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_1;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t385246372* ___SeedArray_2;

public:
	inline static int32_t get_offset_of_inext_0() { return static_cast<int32_t>(offsetof(Random_t108471755, ___inext_0)); }
	inline int32_t get_inext_0() const { return ___inext_0; }
	inline int32_t* get_address_of_inext_0() { return &___inext_0; }
	inline void set_inext_0(int32_t value)
	{
		___inext_0 = value;
	}

	inline static int32_t get_offset_of_inextp_1() { return static_cast<int32_t>(offsetof(Random_t108471755, ___inextp_1)); }
	inline int32_t get_inextp_1() const { return ___inextp_1; }
	inline int32_t* get_address_of_inextp_1() { return &___inextp_1; }
	inline void set_inextp_1(int32_t value)
	{
		___inextp_1 = value;
	}

	inline static int32_t get_offset_of_SeedArray_2() { return static_cast<int32_t>(offsetof(Random_t108471755, ___SeedArray_2)); }
	inline Int32U5BU5D_t385246372* get_SeedArray_2() const { return ___SeedArray_2; }
	inline Int32U5BU5D_t385246372** get_address_of_SeedArray_2() { return &___SeedArray_2; }
	inline void set_SeedArray_2(Int32U5BU5D_t385246372* value)
	{
		___SeedArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___SeedArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T108471755_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CSETUPU3EC__ANONSTOREY0_T1509386468_H
#define U3CSETUPU3EC__ANONSTOREY0_T1509386468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CUIColorPicker/<Setup>c__AnonStorey0
struct  U3CSetupU3Ec__AnonStorey0_t1509386468  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D CUIColorPicker/<Setup>c__AnonStorey0::satvalTex
	Texture2D_t3840446185 * ___satvalTex_0;
	// UnityEngine.Color[] CUIColorPicker/<Setup>c__AnonStorey0::satvalColors
	ColorU5BU5D_t941916413* ___satvalColors_1;
	// System.Single CUIColorPicker/<Setup>c__AnonStorey0::Hue
	float ___Hue_2;
	// UnityEngine.Color[] CUIColorPicker/<Setup>c__AnonStorey0::hueColors
	ColorU5BU5D_t941916413* ___hueColors_3;
	// System.Action CUIColorPicker/<Setup>c__AnonStorey0::resetSatValTexture
	Action_t1264377477 * ___resetSatValTexture_4;
	// System.Single CUIColorPicker/<Setup>c__AnonStorey0::Saturation
	float ___Saturation_5;
	// System.Single CUIColorPicker/<Setup>c__AnonStorey0::Value
	float ___Value_6;
	// UnityEngine.GameObject CUIColorPicker/<Setup>c__AnonStorey0::result
	GameObject_t1113636619 * ___result_7;
	// UnityEngine.GameObject CUIColorPicker/<Setup>c__AnonStorey0::hueGO
	GameObject_t1113636619 * ___hueGO_8;
	// System.Action CUIColorPicker/<Setup>c__AnonStorey0::dragH
	Action_t1264377477 * ___dragH_9;
	// UnityEngine.GameObject CUIColorPicker/<Setup>c__AnonStorey0::satvalGO
	GameObject_t1113636619 * ___satvalGO_10;
	// System.Action CUIColorPicker/<Setup>c__AnonStorey0::dragSV
	Action_t1264377477 * ___dragSV_11;
	// UnityEngine.Vector2 CUIColorPicker/<Setup>c__AnonStorey0::hueSz
	Vector2_t2156229523  ___hueSz_12;
	// System.Action CUIColorPicker/<Setup>c__AnonStorey0::applyHue
	Action_t1264377477 * ___applyHue_13;
	// System.Action CUIColorPicker/<Setup>c__AnonStorey0::applySaturationValue
	Action_t1264377477 * ___applySaturationValue_14;
	// UnityEngine.GameObject CUIColorPicker/<Setup>c__AnonStorey0::hueKnob
	GameObject_t1113636619 * ___hueKnob_15;
	// System.Action CUIColorPicker/<Setup>c__AnonStorey0::idle
	Action_t1264377477 * ___idle_16;
	// UnityEngine.Vector2 CUIColorPicker/<Setup>c__AnonStorey0::satvalSz
	Vector2_t2156229523  ___satvalSz_17;
	// UnityEngine.GameObject CUIColorPicker/<Setup>c__AnonStorey0::satvalKnob
	GameObject_t1113636619 * ___satvalKnob_18;
	// CUIColorPicker CUIColorPicker/<Setup>c__AnonStorey0::$this
	CUIColorPicker_t3452025155 * ___U24this_19;

public:
	inline static int32_t get_offset_of_satvalTex_0() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___satvalTex_0)); }
	inline Texture2D_t3840446185 * get_satvalTex_0() const { return ___satvalTex_0; }
	inline Texture2D_t3840446185 ** get_address_of_satvalTex_0() { return &___satvalTex_0; }
	inline void set_satvalTex_0(Texture2D_t3840446185 * value)
	{
		___satvalTex_0 = value;
		Il2CppCodeGenWriteBarrier((&___satvalTex_0), value);
	}

	inline static int32_t get_offset_of_satvalColors_1() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___satvalColors_1)); }
	inline ColorU5BU5D_t941916413* get_satvalColors_1() const { return ___satvalColors_1; }
	inline ColorU5BU5D_t941916413** get_address_of_satvalColors_1() { return &___satvalColors_1; }
	inline void set_satvalColors_1(ColorU5BU5D_t941916413* value)
	{
		___satvalColors_1 = value;
		Il2CppCodeGenWriteBarrier((&___satvalColors_1), value);
	}

	inline static int32_t get_offset_of_Hue_2() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___Hue_2)); }
	inline float get_Hue_2() const { return ___Hue_2; }
	inline float* get_address_of_Hue_2() { return &___Hue_2; }
	inline void set_Hue_2(float value)
	{
		___Hue_2 = value;
	}

	inline static int32_t get_offset_of_hueColors_3() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___hueColors_3)); }
	inline ColorU5BU5D_t941916413* get_hueColors_3() const { return ___hueColors_3; }
	inline ColorU5BU5D_t941916413** get_address_of_hueColors_3() { return &___hueColors_3; }
	inline void set_hueColors_3(ColorU5BU5D_t941916413* value)
	{
		___hueColors_3 = value;
		Il2CppCodeGenWriteBarrier((&___hueColors_3), value);
	}

	inline static int32_t get_offset_of_resetSatValTexture_4() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___resetSatValTexture_4)); }
	inline Action_t1264377477 * get_resetSatValTexture_4() const { return ___resetSatValTexture_4; }
	inline Action_t1264377477 ** get_address_of_resetSatValTexture_4() { return &___resetSatValTexture_4; }
	inline void set_resetSatValTexture_4(Action_t1264377477 * value)
	{
		___resetSatValTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___resetSatValTexture_4), value);
	}

	inline static int32_t get_offset_of_Saturation_5() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___Saturation_5)); }
	inline float get_Saturation_5() const { return ___Saturation_5; }
	inline float* get_address_of_Saturation_5() { return &___Saturation_5; }
	inline void set_Saturation_5(float value)
	{
		___Saturation_5 = value;
	}

	inline static int32_t get_offset_of_Value_6() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___Value_6)); }
	inline float get_Value_6() const { return ___Value_6; }
	inline float* get_address_of_Value_6() { return &___Value_6; }
	inline void set_Value_6(float value)
	{
		___Value_6 = value;
	}

	inline static int32_t get_offset_of_result_7() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___result_7)); }
	inline GameObject_t1113636619 * get_result_7() const { return ___result_7; }
	inline GameObject_t1113636619 ** get_address_of_result_7() { return &___result_7; }
	inline void set_result_7(GameObject_t1113636619 * value)
	{
		___result_7 = value;
		Il2CppCodeGenWriteBarrier((&___result_7), value);
	}

	inline static int32_t get_offset_of_hueGO_8() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___hueGO_8)); }
	inline GameObject_t1113636619 * get_hueGO_8() const { return ___hueGO_8; }
	inline GameObject_t1113636619 ** get_address_of_hueGO_8() { return &___hueGO_8; }
	inline void set_hueGO_8(GameObject_t1113636619 * value)
	{
		___hueGO_8 = value;
		Il2CppCodeGenWriteBarrier((&___hueGO_8), value);
	}

	inline static int32_t get_offset_of_dragH_9() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___dragH_9)); }
	inline Action_t1264377477 * get_dragH_9() const { return ___dragH_9; }
	inline Action_t1264377477 ** get_address_of_dragH_9() { return &___dragH_9; }
	inline void set_dragH_9(Action_t1264377477 * value)
	{
		___dragH_9 = value;
		Il2CppCodeGenWriteBarrier((&___dragH_9), value);
	}

	inline static int32_t get_offset_of_satvalGO_10() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___satvalGO_10)); }
	inline GameObject_t1113636619 * get_satvalGO_10() const { return ___satvalGO_10; }
	inline GameObject_t1113636619 ** get_address_of_satvalGO_10() { return &___satvalGO_10; }
	inline void set_satvalGO_10(GameObject_t1113636619 * value)
	{
		___satvalGO_10 = value;
		Il2CppCodeGenWriteBarrier((&___satvalGO_10), value);
	}

	inline static int32_t get_offset_of_dragSV_11() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___dragSV_11)); }
	inline Action_t1264377477 * get_dragSV_11() const { return ___dragSV_11; }
	inline Action_t1264377477 ** get_address_of_dragSV_11() { return &___dragSV_11; }
	inline void set_dragSV_11(Action_t1264377477 * value)
	{
		___dragSV_11 = value;
		Il2CppCodeGenWriteBarrier((&___dragSV_11), value);
	}

	inline static int32_t get_offset_of_hueSz_12() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___hueSz_12)); }
	inline Vector2_t2156229523  get_hueSz_12() const { return ___hueSz_12; }
	inline Vector2_t2156229523 * get_address_of_hueSz_12() { return &___hueSz_12; }
	inline void set_hueSz_12(Vector2_t2156229523  value)
	{
		___hueSz_12 = value;
	}

	inline static int32_t get_offset_of_applyHue_13() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___applyHue_13)); }
	inline Action_t1264377477 * get_applyHue_13() const { return ___applyHue_13; }
	inline Action_t1264377477 ** get_address_of_applyHue_13() { return &___applyHue_13; }
	inline void set_applyHue_13(Action_t1264377477 * value)
	{
		___applyHue_13 = value;
		Il2CppCodeGenWriteBarrier((&___applyHue_13), value);
	}

	inline static int32_t get_offset_of_applySaturationValue_14() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___applySaturationValue_14)); }
	inline Action_t1264377477 * get_applySaturationValue_14() const { return ___applySaturationValue_14; }
	inline Action_t1264377477 ** get_address_of_applySaturationValue_14() { return &___applySaturationValue_14; }
	inline void set_applySaturationValue_14(Action_t1264377477 * value)
	{
		___applySaturationValue_14 = value;
		Il2CppCodeGenWriteBarrier((&___applySaturationValue_14), value);
	}

	inline static int32_t get_offset_of_hueKnob_15() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___hueKnob_15)); }
	inline GameObject_t1113636619 * get_hueKnob_15() const { return ___hueKnob_15; }
	inline GameObject_t1113636619 ** get_address_of_hueKnob_15() { return &___hueKnob_15; }
	inline void set_hueKnob_15(GameObject_t1113636619 * value)
	{
		___hueKnob_15 = value;
		Il2CppCodeGenWriteBarrier((&___hueKnob_15), value);
	}

	inline static int32_t get_offset_of_idle_16() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___idle_16)); }
	inline Action_t1264377477 * get_idle_16() const { return ___idle_16; }
	inline Action_t1264377477 ** get_address_of_idle_16() { return &___idle_16; }
	inline void set_idle_16(Action_t1264377477 * value)
	{
		___idle_16 = value;
		Il2CppCodeGenWriteBarrier((&___idle_16), value);
	}

	inline static int32_t get_offset_of_satvalSz_17() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___satvalSz_17)); }
	inline Vector2_t2156229523  get_satvalSz_17() const { return ___satvalSz_17; }
	inline Vector2_t2156229523 * get_address_of_satvalSz_17() { return &___satvalSz_17; }
	inline void set_satvalSz_17(Vector2_t2156229523  value)
	{
		___satvalSz_17 = value;
	}

	inline static int32_t get_offset_of_satvalKnob_18() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___satvalKnob_18)); }
	inline GameObject_t1113636619 * get_satvalKnob_18() const { return ___satvalKnob_18; }
	inline GameObject_t1113636619 ** get_address_of_satvalKnob_18() { return &___satvalKnob_18; }
	inline void set_satvalKnob_18(GameObject_t1113636619 * value)
	{
		___satvalKnob_18 = value;
		Il2CppCodeGenWriteBarrier((&___satvalKnob_18), value);
	}

	inline static int32_t get_offset_of_U24this_19() { return static_cast<int32_t>(offsetof(U3CSetupU3Ec__AnonStorey0_t1509386468, ___U24this_19)); }
	inline CUIColorPicker_t3452025155 * get_U24this_19() const { return ___U24this_19; }
	inline CUIColorPicker_t3452025155 ** get_address_of_U24this_19() { return &___U24this_19; }
	inline void set_U24this_19(CUIColorPicker_t3452025155 * value)
	{
		___U24this_19 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETUPU3EC__ANONSTOREY0_T1509386468_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef FILEATTRIBUTES_T3417205536_H
#define FILEATTRIBUTES_T3417205536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAttributes
struct  FileAttributes_t3417205536 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAttributes_t3417205536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEATTRIBUTES_T3417205536_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MONOIOSTAT_T592533987_H
#define MONOIOSTAT_T592533987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOStat
struct  MonoIOStat_t592533987 
{
public:
	// System.String System.IO.MonoIOStat::Name
	String_t* ___Name_0;
	// System.IO.FileAttributes System.IO.MonoIOStat::Attributes
	int32_t ___Attributes_1;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_2;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_3;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_4;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Attributes_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Attributes_1)); }
	inline int32_t get_Attributes_1() const { return ___Attributes_1; }
	inline int32_t* get_address_of_Attributes_1() { return &___Attributes_1; }
	inline void set_Attributes_1(int32_t value)
	{
		___Attributes_1 = value;
	}

	inline static int32_t get_offset_of_Length_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___Length_2)); }
	inline int64_t get_Length_2() const { return ___Length_2; }
	inline int64_t* get_address_of_Length_2() { return &___Length_2; }
	inline void set_Length_2(int64_t value)
	{
		___Length_2 = value;
	}

	inline static int32_t get_offset_of_CreationTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___CreationTime_3)); }
	inline int64_t get_CreationTime_3() const { return ___CreationTime_3; }
	inline int64_t* get_address_of_CreationTime_3() { return &___CreationTime_3; }
	inline void set_CreationTime_3(int64_t value)
	{
		___CreationTime_3 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastAccessTime_4)); }
	inline int64_t get_LastAccessTime_4() const { return ___LastAccessTime_4; }
	inline int64_t* get_address_of_LastAccessTime_4() { return &___LastAccessTime_4; }
	inline void set_LastAccessTime_4(int64_t value)
	{
		___LastAccessTime_4 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_5() { return static_cast<int32_t>(offsetof(MonoIOStat_t592533987, ___LastWriteTime_5)); }
	inline int64_t get_LastWriteTime_5() const { return ___LastWriteTime_5; }
	inline int64_t* get_address_of_LastWriteTime_5() { return &___LastWriteTime_5; }
	inline void set_LastWriteTime_5(int64_t value)
	{
		___LastWriteTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.IO.MonoIOStat
struct MonoIOStat_t592533987_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
// Native definition for COM marshalling of System.IO.MonoIOStat
struct MonoIOStat_t592533987_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___Attributes_1;
	int64_t ___Length_2;
	int64_t ___CreationTime_3;
	int64_t ___LastAccessTime_4;
	int64_t ___LastWriteTime_5;
};
#endif // MONOIOSTAT_T592533987_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef BRUSHFUNCTION_T136629315_H
#define BRUSHFUNCTION_T136629315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Painter/BrushFunction
struct  BrushFunction_t136629315  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSHFUNCTION_T136629315_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef ACTION_1_T2728153919_H
#define ACTION_1_T2728153919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.Color>
struct  Action_1_t2728153919  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2728153919_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef FILESYSTEMINFO_T3745885336_H
#define FILESYSTEMINFO_T3745885336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileSystemInfo
struct  FileSystemInfo_t3745885336  : public MarshalByRefObject_t2760389100
{
public:
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_1;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_2;
	// System.IO.MonoIOStat System.IO.FileSystemInfo::stat
	MonoIOStat_t592533987  ___stat_3;
	// System.Boolean System.IO.FileSystemInfo::valid
	bool ___valid_4;

public:
	inline static int32_t get_offset_of_FullPath_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___FullPath_1)); }
	inline String_t* get_FullPath_1() const { return ___FullPath_1; }
	inline String_t** get_address_of_FullPath_1() { return &___FullPath_1; }
	inline void set_FullPath_1(String_t* value)
	{
		___FullPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___FullPath_1), value);
	}

	inline static int32_t get_offset_of_OriginalPath_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___OriginalPath_2)); }
	inline String_t* get_OriginalPath_2() const { return ___OriginalPath_2; }
	inline String_t** get_address_of_OriginalPath_2() { return &___OriginalPath_2; }
	inline void set_OriginalPath_2(String_t* value)
	{
		___OriginalPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalPath_2), value);
	}

	inline static int32_t get_offset_of_stat_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___stat_3)); }
	inline MonoIOStat_t592533987  get_stat_3() const { return ___stat_3; }
	inline MonoIOStat_t592533987 * get_address_of_stat_3() { return &___stat_3; }
	inline void set_stat_3(MonoIOStat_t592533987  value)
	{
		___stat_3 = value;
	}

	inline static int32_t get_offset_of_valid_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t3745885336, ___valid_4)); }
	inline bool get_valid_4() const { return ___valid_4; }
	inline bool* get_address_of_valid_4() { return &___valid_4; }
	inline void set_valid_4(bool value)
	{
		___valid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESYSTEMINFO_T3745885336_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef DIRECTORYINFO_T35957480_H
#define DIRECTORYINFO_T35957480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.DirectoryInfo
struct  DirectoryInfo_t35957480  : public FileSystemInfo_t3745885336
{
public:
	// System.String System.IO.DirectoryInfo::current
	String_t* ___current_5;
	// System.String System.IO.DirectoryInfo::parent
	String_t* ___parent_6;

public:
	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___current_5)); }
	inline String_t* get_current_5() const { return ___current_5; }
	inline String_t** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(String_t* value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(DirectoryInfo_t35957480, ___parent_6)); }
	inline String_t* get_parent_6() const { return ___parent_6; }
	inline String_t** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(String_t* value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___parent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORYINFO_T35957480_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t190067161 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t190067161 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t190067161 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t190067161 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t190067161 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t190067161 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_5;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_5() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_5)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_5() const { return ___reapplyDrivenProperties_5; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_5() { return &___reapplyDrivenProperties_5; }
	inline void set_reapplyDrivenProperties_5(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_5 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef CUICOLORPICKER_T3452025155_H
#define CUICOLORPICKER_T3452025155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CUIColorPicker
struct  CUIColorPicker_t3452025155  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color CUIColorPicker::_color
	Color_t2555686324  ____color_4;
	// System.Action`1<UnityEngine.Color> CUIColorPicker::_onValueChange
	Action_1_t2728153919 * ____onValueChange_5;
	// System.Action CUIColorPicker::_update
	Action_t1264377477 * ____update_6;

public:
	inline static int32_t get_offset_of__color_4() { return static_cast<int32_t>(offsetof(CUIColorPicker_t3452025155, ____color_4)); }
	inline Color_t2555686324  get__color_4() const { return ____color_4; }
	inline Color_t2555686324 * get_address_of__color_4() { return &____color_4; }
	inline void set__color_4(Color_t2555686324  value)
	{
		____color_4 = value;
	}

	inline static int32_t get_offset_of__onValueChange_5() { return static_cast<int32_t>(offsetof(CUIColorPicker_t3452025155, ____onValueChange_5)); }
	inline Action_1_t2728153919 * get__onValueChange_5() const { return ____onValueChange_5; }
	inline Action_1_t2728153919 ** get_address_of__onValueChange_5() { return &____onValueChange_5; }
	inline void set__onValueChange_5(Action_1_t2728153919 * value)
	{
		____onValueChange_5 = value;
		Il2CppCodeGenWriteBarrier((&____onValueChange_5), value);
	}

	inline static int32_t get_offset_of__update_6() { return static_cast<int32_t>(offsetof(CUIColorPicker_t3452025155, ____update_6)); }
	inline Action_t1264377477 * get__update_6() const { return ____update_6; }
	inline Action_t1264377477 ** get_address_of__update_6() { return &____update_6; }
	inline void set__update_6(Action_t1264377477 * value)
	{
		____update_6 = value;
		Il2CppCodeGenWriteBarrier((&____update_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUICOLORPICKER_T3452025155_H
#ifndef PAINTER_T2372114961_H
#define PAINTER_T2372114961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Painter
struct  Painter_t2372114961  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.LayerMask Painter::_drawingLayers
	LayerMask_t3493934918  ____drawingLayers_7;
	// Painter/BrushFunction Painter::_currentBrush
	BrushFunction_t136629315 * ____currentBrush_8;
	// UnityEngine.Color Painter::_resetColour
	Color_t2555686324  ____resetColour_9;
	// UnityEngine.Color[] Painter::_cleanColoursArray
	ColorU5BU5D_t941916413* ____cleanColoursArray_10;
	// UnityEngine.Color32[] Painter::_curentColors
	Color32U5BU5D_t3850468773* ____curentColors_11;
	// UnityEngine.Sprite Painter::_drawableSprite
	Sprite_t280657092 * ____drawableSprite_12;
	// UnityEngine.Texture2D Painter::_drawableTexture
	Texture2D_t3840446185 * ____drawableTexture_13;
	// UnityEngine.Vector2 Painter::_previousDragPosition
	Vector2_t2156229523  ____previousDragPosition_14;

public:
	inline static int32_t get_offset_of__drawingLayers_7() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____drawingLayers_7)); }
	inline LayerMask_t3493934918  get__drawingLayers_7() const { return ____drawingLayers_7; }
	inline LayerMask_t3493934918 * get_address_of__drawingLayers_7() { return &____drawingLayers_7; }
	inline void set__drawingLayers_7(LayerMask_t3493934918  value)
	{
		____drawingLayers_7 = value;
	}

	inline static int32_t get_offset_of__currentBrush_8() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____currentBrush_8)); }
	inline BrushFunction_t136629315 * get__currentBrush_8() const { return ____currentBrush_8; }
	inline BrushFunction_t136629315 ** get_address_of__currentBrush_8() { return &____currentBrush_8; }
	inline void set__currentBrush_8(BrushFunction_t136629315 * value)
	{
		____currentBrush_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentBrush_8), value);
	}

	inline static int32_t get_offset_of__resetColour_9() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____resetColour_9)); }
	inline Color_t2555686324  get__resetColour_9() const { return ____resetColour_9; }
	inline Color_t2555686324 * get_address_of__resetColour_9() { return &____resetColour_9; }
	inline void set__resetColour_9(Color_t2555686324  value)
	{
		____resetColour_9 = value;
	}

	inline static int32_t get_offset_of__cleanColoursArray_10() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____cleanColoursArray_10)); }
	inline ColorU5BU5D_t941916413* get__cleanColoursArray_10() const { return ____cleanColoursArray_10; }
	inline ColorU5BU5D_t941916413** get_address_of__cleanColoursArray_10() { return &____cleanColoursArray_10; }
	inline void set__cleanColoursArray_10(ColorU5BU5D_t941916413* value)
	{
		____cleanColoursArray_10 = value;
		Il2CppCodeGenWriteBarrier((&____cleanColoursArray_10), value);
	}

	inline static int32_t get_offset_of__curentColors_11() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____curentColors_11)); }
	inline Color32U5BU5D_t3850468773* get__curentColors_11() const { return ____curentColors_11; }
	inline Color32U5BU5D_t3850468773** get_address_of__curentColors_11() { return &____curentColors_11; }
	inline void set__curentColors_11(Color32U5BU5D_t3850468773* value)
	{
		____curentColors_11 = value;
		Il2CppCodeGenWriteBarrier((&____curentColors_11), value);
	}

	inline static int32_t get_offset_of__drawableSprite_12() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____drawableSprite_12)); }
	inline Sprite_t280657092 * get__drawableSprite_12() const { return ____drawableSprite_12; }
	inline Sprite_t280657092 ** get_address_of__drawableSprite_12() { return &____drawableSprite_12; }
	inline void set__drawableSprite_12(Sprite_t280657092 * value)
	{
		____drawableSprite_12 = value;
		Il2CppCodeGenWriteBarrier((&____drawableSprite_12), value);
	}

	inline static int32_t get_offset_of__drawableTexture_13() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____drawableTexture_13)); }
	inline Texture2D_t3840446185 * get__drawableTexture_13() const { return ____drawableTexture_13; }
	inline Texture2D_t3840446185 ** get_address_of__drawableTexture_13() { return &____drawableTexture_13; }
	inline void set__drawableTexture_13(Texture2D_t3840446185 * value)
	{
		____drawableTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&____drawableTexture_13), value);
	}

	inline static int32_t get_offset_of__previousDragPosition_14() { return static_cast<int32_t>(offsetof(Painter_t2372114961, ____previousDragPosition_14)); }
	inline Vector2_t2156229523  get__previousDragPosition_14() const { return ____previousDragPosition_14; }
	inline Vector2_t2156229523 * get_address_of__previousDragPosition_14() { return &____previousDragPosition_14; }
	inline void set__previousDragPosition_14(Vector2_t2156229523  value)
	{
		____previousDragPosition_14 = value;
	}
};

struct Painter_t2372114961_StaticFields
{
public:
	// UnityEngine.Color Painter::PenColour
	Color_t2555686324  ___PenColour_4;
	// System.Int32 Painter::PenWidth
	int32_t ___PenWidth_5;
	// Painter Painter::_drawable
	Painter_t2372114961 * ____drawable_6;

public:
	inline static int32_t get_offset_of_PenColour_4() { return static_cast<int32_t>(offsetof(Painter_t2372114961_StaticFields, ___PenColour_4)); }
	inline Color_t2555686324  get_PenColour_4() const { return ___PenColour_4; }
	inline Color_t2555686324 * get_address_of_PenColour_4() { return &___PenColour_4; }
	inline void set_PenColour_4(Color_t2555686324  value)
	{
		___PenColour_4 = value;
	}

	inline static int32_t get_offset_of_PenWidth_5() { return static_cast<int32_t>(offsetof(Painter_t2372114961_StaticFields, ___PenWidth_5)); }
	inline int32_t get_PenWidth_5() const { return ___PenWidth_5; }
	inline int32_t* get_address_of_PenWidth_5() { return &___PenWidth_5; }
	inline void set_PenWidth_5(int32_t value)
	{
		___PenWidth_5 = value;
	}

	inline static int32_t get_offset_of__drawable_6() { return static_cast<int32_t>(offsetof(Painter_t2372114961_StaticFields, ____drawable_6)); }
	inline Painter_t2372114961 * get__drawable_6() const { return ____drawable_6; }
	inline Painter_t2372114961 ** get_address_of__drawable_6() { return &____drawable_6; }
	inline void set__drawable_6(Painter_t2372114961 * value)
	{
		____drawable_6 = value;
		Il2CppCodeGenWriteBarrier((&____drawable_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAINTER_T2372114961_H
#ifndef PAINTINGSETTINGS_T3763250382_H
#define PAINTINGSETTINGS_T3763250382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PaintingSettings
struct  PaintingSettings_t3763250382  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PaintingSettings::_transparency
	float ____transparency_4;
	// CUIColorPicker PaintingSettings::_cUIColorPicker
	CUIColorPicker_t3452025155 * ____cUIColorPicker_5;

public:
	inline static int32_t get_offset_of__transparency_4() { return static_cast<int32_t>(offsetof(PaintingSettings_t3763250382, ____transparency_4)); }
	inline float get__transparency_4() const { return ____transparency_4; }
	inline float* get_address_of__transparency_4() { return &____transparency_4; }
	inline void set__transparency_4(float value)
	{
		____transparency_4 = value;
	}

	inline static int32_t get_offset_of__cUIColorPicker_5() { return static_cast<int32_t>(offsetof(PaintingSettings_t3763250382, ____cUIColorPicker_5)); }
	inline CUIColorPicker_t3452025155 * get__cUIColorPicker_5() const { return ____cUIColorPicker_5; }
	inline CUIColorPicker_t3452025155 ** get_address_of__cUIColorPicker_5() { return &____cUIColorPicker_5; }
	inline void set__cUIColorPicker_5(CUIColorPicker_t3452025155 * value)
	{
		____cUIColorPicker_5 = value;
		Il2CppCodeGenWriteBarrier((&____cUIColorPicker_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAINTINGSETTINGS_T3763250382_H
#ifndef SAVEHEALPER_T3871815534_H
#define SAVEHEALPER_T3871815534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveHealper
struct  SaveHealper_t3871815534  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEHEALPER_T3871815534_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_31)); }
	inline Sprite_t280657092 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_t280657092 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_32)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_42;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_43;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_45;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_46;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_47;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_48;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_42)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_42() const { return ___s_VertScratch_42; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_42() { return &___s_VertScratch_42; }
	inline void set_s_VertScratch_42(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_42), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_43)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_43() const { return ___s_UVScratch_43; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_43() { return &___s_UVScratch_43; }
	inline void set_s_UVScratch_43(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_43), value);
	}

	inline static int32_t get_offset_of_s_Xy_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_44)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_44() const { return ___s_Xy_44; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_44() { return &___s_Xy_44; }
	inline void set_s_Xy_44(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_44), value);
	}

	inline static int32_t get_offset_of_s_Uv_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_45)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_45() const { return ___s_Uv_45; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_45() { return &___s_Uv_45; }
	inline void set_s_Uv_45(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_45), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_46)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_46() const { return ___m_TrackedTexturelessImages_46; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_46() { return &___m_TrackedTexturelessImages_46; }
	inline void set_m_TrackedTexturelessImages_46(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_46), value);
	}

	inline static int32_t get_offset_of_s_Initialized_47() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_47)); }
	inline bool get_s_Initialized_47() const { return ___s_Initialized_47; }
	inline bool* get_address_of_s_Initialized_47() { return &___s_Initialized_47; }
	inline void set_s_Initialized_47(bool value)
	{
		___s_Initialized_47 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_48() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_48)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_48() const { return ___U3CU3Ef__mgU24cache0_48; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_48() { return &___U3CU3Ef__mgU24cache0_48; }
	inline void set_U3CU3Ef__mgU24cache0_48(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// System.Single[]
struct SingleU5BU5D_t1444911251  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t2600501292  m_Items[1];

public:
	inline Color32_t2600501292  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t2600501292 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t2600501292  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t2600501292  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t2600501292 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t2600501292  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.Color>::Invoke(!0)
extern "C" IL2CPP_METHOD_ATTR void Action_1_Invoke_m3561268814_gshared (Action_1_t2728153919 * __this, Color_t2555686324  p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);

// UnityEngine.Color UnityEngine.Color::get_red()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_red_m3227813939 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void CUIColorPicker::Setup(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_Setup_m1889472872 (CUIColorPicker_t3452025155 * __this, Color_t2555686324  ___inputColor0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single[])
extern "C" IL2CPP_METHOD_ATTR float Mathf_Min_m209631461 (RuntimeObject * __this /* static, unused */, SingleU5BU5D_t1444911251* p0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single[])
extern "C" IL2CPP_METHOD_ATTR float Mathf_Max_m1211304751 (RuntimeObject * __this /* static, unused */, SingleU5BU5D_t1444911251* p0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Repeat_m1502810009 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_InverseTransformPoint_m1343916000 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C" IL2CPP_METHOD_ATTR Rect_t2360479859  RectTransform_get_rect_m574169965 (RectTransform_t3704657025 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Rect_get_min_m847841034 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Rect_get_max_m350137314 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Rect_Contains_m1232228501 (Rect_t2360479859 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Rect_get_size_m477575021 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0__ctor_m2873649314 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject CUIColorPicker::GO(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * CUIColorPicker_GO_m3538182937 (CUIColorPicker_t3452025155 * __this, String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_yellow_m1287957903 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_green_m490390750 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_cyan()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_cyan_m765383084 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_blue()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_blue_m3190273327 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_magenta()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_magenta_m208363462 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m373113269 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixel_m2984741184 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, Color_t2555686324  p2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
inline Image_t2670269651 * GameObject_GetComponent_TisImage_t2670269651_m2486712510 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Sprite_t280657092 * Sprite_Create_m803022218 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, Rect_t2360479859  p1, Vector2_t2156229523  p2, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// UnityEngine.Vector2 CUIColorPicker::GetWidgetSize(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  CUIColorPicker_GetWidgetSize_m2427733028 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m2994342681 (Action_t1264377477 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void CUIColorPicker::RGBToHSV(UnityEngine.Color,System.Single&,System.Single&,System.Single&)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_RGBToHSV_m132274398 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___color0, float* ___h1, float* ___s2, float* ___v3, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Action_Invoke_m937035532 (Action_t1264377477 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_localPosition_m4234289348 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void System.Random::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Random__ctor_m4122933043 (Random_t108471755 * __this, const RuntimeMethod* method);
// System.Void CUIColorPicker::set_Color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_set_Color_m30073458 (CUIColorPicker_t3452025155 * __this, Color_t2555686324  ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_Clamp_m2756574208 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_Lerp_m973389909 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, float p2, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Multiply(System.Single,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_op_Multiply_m2768890077 (RuntimeObject * __this /* static, unused */, float p0, Color_t2555686324  p1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Addition(UnityEngine.Color,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_op_Addition_m3293657895 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Color::op_Inequality(UnityEngine.Color,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR bool Color_op_Inequality_m3353772181 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.Color>::Invoke(!0)
inline void Action_1_Invoke_m3561268814 (Action_1_t2728153919 * __this, Color_t2555686324  p0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t2728153919 *, Color_t2555686324 , const RuntimeMethod*))Action_1_Invoke_m3561268814_gshared)(__this, p0, method);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m2081676745 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Boolean CUIColorPicker::GetLocalMouse(UnityEngine.GameObject,UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR bool CUIColorPicker_GetLocalMouse_m3410064497 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, Vector2_t2156229523 * ___result1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetMouseButtonUp_m2924350851 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Painter/BrushFunction::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void BrushFunction__ctor_m2232455995 (BrushFunction_t136629315 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3235626157 * Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3235626157 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C" IL2CPP_METHOD_ATTR Sprite_t280657092 * SpriteRenderer_get_sprite_m663386871 (SpriteRenderer_t3235626157 * __this, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C" IL2CPP_METHOD_ATTR Texture2D_t3840446185 * Sprite_get_texture_m3976398399 (Sprite_t280657092 * __this, const RuntimeMethod* method);
// System.Void Painter::ResetCanvas()
extern "C" IL2CPP_METHOD_ATTR void Painter_ResetCanvas_m1980404891 (Painter_t2372114961 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetMouseButton_m513753021 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Camera_ScreenToWorldPoint_m3978588570 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C" IL2CPP_METHOD_ATTR int32_t LayerMask_get_value_m1881709263 (LayerMask_t3493934918 * __this, const RuntimeMethod* method);
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Collider2D_t2806799626 * Physics2D_OverlapPoint_m3522226740 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void Painter/BrushFunction::Invoke(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void BrushFunction_Invoke_m4261340326 (BrushFunction_t136629315 * __this, Vector2_t2156229523  ___world_position0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector2 Painter::WorldToPixelCoordinates(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Painter_WorldToPixelCoordinates_m3555177886 (Painter_t2372114961 * __this, Vector2_t2156229523  ____worldPosition0, const RuntimeMethod* method);
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern "C" IL2CPP_METHOD_ATTR Color32U5BU5D_t3850468773* Texture2D_GetPixels32_m647746242 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m2303255133 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// System.Void Painter::MarkPixelsToColour(UnityEngine.Vector2,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_MarkPixelsToColour_m2454846674 (Painter_t2372114961 * __this, Vector2_t2156229523  ____centerPixel0, int32_t ___penThickness1, Color_t2555686324  ____penColor2, const RuntimeMethod* method);
// System.Void Painter::Interpolation(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_Interpolation_m2325092361 (Painter_t2372114961 * __this, Vector2_t2156229523  ____startPoint0, Vector2_t2156229523  ____endPoint1, int32_t ____width2, Color_t2555686324  ____color3, const RuntimeMethod* method);
// System.Void Painter::ApplyMarkedPixelChanges()
extern "C" IL2CPP_METHOD_ATTR void Painter_ApplyMarkedPixelChanges_m581631983 (Painter_t2372114961 * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C" IL2CPP_METHOD_ATTR Rect_t2360479859  Sprite_get_rect_m2575211689 (Sprite_t280657092 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_width_m3421484486 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// System.Void Painter::MarkPixelToChange(System.Int32,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_MarkPixelToChange_m2658964883 (Painter_t2372114961 * __this, int32_t ___x0, int32_t ___y1, Color_t2555686324  ____color2, const RuntimeMethod* method);
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR Color32_t2600501292  Color32_op_Implicit_m2658259763 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixels32_m1141065075 (Texture2D_t3840446185 * __this, Color32U5BU5D_t3850468773* p0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Vector2_Distance_m3048868881 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_normalized_m2683665860 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_Lerp_m854472224 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, float p2, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Sprite::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  Sprite_get_bounds_m2438297458 (Sprite_t280657092 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Bounds_get_size_m1178783246 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_localScale_m129152068 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m1874334613 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixels_m3008871897 (Texture2D_t3840446185 * __this, ColorU5BU5D_t941916413* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<CUIColorPicker>()
inline CUIColorPicker_t3452025155 * Object_FindObjectOfType_TisCUIColorPicker_t3452025155_m1733115665 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  CUIColorPicker_t3452025155 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method);
}
// System.Void PaintingSettings::SetMarkerWidth(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetMarkerWidth_m3501128933 (PaintingSettings_t3763250382 * __this, int32_t ___new_width0, const RuntimeMethod* method);
// UnityEngine.Color CUIColorPicker::get_Color()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  CUIColorPicker_get_Color_m3988971789 (CUIColorPicker_t3452025155 * __this, const RuntimeMethod* method);
// System.Void PaintingSettings::SetMarkerColour(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetMarkerColour_m484989563 (PaintingSettings_t3763250382 * __this, Color_t2555686324  ___new_color0, const RuntimeMethod* method);
// System.Void Painter::SetPenBrush()
extern "C" IL2CPP_METHOD_ATTR void Painter_SetPenBrush_m424290498 (Painter_t2372114961 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* ImageConversion_EncodeToPNG_m2292631531 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_dataPath()
extern "C" IL2CPP_METHOD_ATTR String_t* Application_get_dataPath_m4232621142 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean System.IO.Directory::Exists(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Directory_Exists_m1484791558 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
extern "C" IL2CPP_METHOD_ATTR DirectoryInfo_t35957480 * Directory_CreateDirectory_m751642867 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
extern "C" IL2CPP_METHOD_ATTR DateTime_t3738529785  DateTime_get_Now_m1277138875 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.DateTime::ToString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* DateTime_ToString_m3718521780 (DateTime_t3738529785 * __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2163913788 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method);
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void File_WriteAllBytes_m4252682105 (RuntimeObject * __this /* static, unused */, String_t* p0, ByteU5BU5D_t4116647657* p1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CUIColorPicker::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker__ctor_m807708131 (CUIColorPicker_t3452025155 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__color_4(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color CUIColorPicker::get_Color()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  CUIColorPicker_get_Color_m3988971789 (CUIColorPicker_t3452025155 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0 = __this->get__color_4();
		return L_0;
	}
}
// System.Void CUIColorPicker::set_Color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_set_Color_m30073458 (CUIColorPicker_t3452025155 * __this, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0 = ___value0;
		CUIColorPicker_Setup_m1889472872(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CUIColorPicker::SetOnValueChangeCallback(System.Action`1<UnityEngine.Color>)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_SetOnValueChangeCallback_m258837842 (CUIColorPicker_t3452025155 * __this, Action_1_t2728153919 * ___onValueChange0, const RuntimeMethod* method)
{
	{
		Action_1_t2728153919 * L_0 = ___onValueChange0;
		__this->set__onValueChange_5(L_0);
		return;
	}
}
// System.Void CUIColorPicker::RGBToHSV(UnityEngine.Color,System.Single&,System.Single&,System.Single&)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_RGBToHSV_m132274398 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___color0, float* ___h1, float* ___s2, float* ___v3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CUIColorPicker_RGBToHSV_m132274398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float* G_B9_0 = NULL;
	float* G_B8_0 = NULL;
	float G_B10_0 = 0.0f;
	float* G_B10_1 = NULL;
	{
		SingleU5BU5D_t1444911251* L_0 = (SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)3);
		SingleU5BU5D_t1444911251* L_1 = L_0;
		float L_2 = (&___color0)->get_r_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t1444911251* L_3 = L_1;
		float L_4 = (&___color0)->get_g_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t1444911251* L_5 = L_3;
		float L_6 = (&___color0)->get_b_2();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m209631461(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_7;
		SingleU5BU5D_t1444911251* L_8 = (SingleU5BU5D_t1444911251*)SZArrayNew(SingleU5BU5D_t1444911251_il2cpp_TypeInfo_var, (uint32_t)3);
		SingleU5BU5D_t1444911251* L_9 = L_8;
		float L_10 = (&___color0)->get_r_0();
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_10);
		SingleU5BU5D_t1444911251* L_11 = L_9;
		float L_12 = (&___color0)->get_g_1();
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_12);
		SingleU5BU5D_t1444911251* L_13 = L_11;
		float L_14 = (&___color0)->get_b_2();
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_14);
		float L_15 = Mathf_Max_m1211304751(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = V_1;
		float L_17 = V_0;
		V_2 = ((float)il2cpp_codegen_subtract((float)L_16, (float)L_17));
		float L_18 = V_2;
		if ((!(((float)L_18) == ((float)(0.0f)))))
		{
			goto IL_006f;
		}
	}
	{
		float* L_19 = ___h1;
		*((float*)(L_19)) = (float)(0.0f);
		goto IL_00e2;
	}

IL_006f:
	{
		float L_20 = V_1;
		float L_21 = (&___color0)->get_r_0();
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_009e;
		}
	}
	{
		float* L_22 = ___h1;
		float L_23 = (&___color0)->get_g_1();
		float L_24 = (&___color0)->get_b_2();
		float L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_26 = Mathf_Repeat_m1502810009(NULL /*static, unused*/, ((float)((float)((float)il2cpp_codegen_subtract((float)L_23, (float)L_24))/(float)L_25)), (6.0f), /*hidden argument*/NULL);
		*((float*)(L_22)) = (float)L_26;
		goto IL_00e2;
	}

IL_009e:
	{
		float L_27 = V_1;
		float L_28 = (&___color0)->get_g_1();
		if ((!(((float)L_27) == ((float)L_28))))
		{
			goto IL_00c9;
		}
	}
	{
		float* L_29 = ___h1;
		float L_30 = (&___color0)->get_b_2();
		float L_31 = (&___color0)->get_r_0();
		float L_32 = V_2;
		*((float*)(L_29)) = (float)((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_subtract((float)L_30, (float)L_31))/(float)L_32)), (float)(2.0f)));
		goto IL_00e2;
	}

IL_00c9:
	{
		float* L_33 = ___h1;
		float L_34 = (&___color0)->get_r_0();
		float L_35 = (&___color0)->get_g_1();
		float L_36 = V_2;
		*((float*)(L_33)) = (float)((float)il2cpp_codegen_add((float)((float)((float)((float)il2cpp_codegen_subtract((float)L_34, (float)L_35))/(float)L_36)), (float)(4.0f)));
	}

IL_00e2:
	{
		float* L_37 = ___s2;
		float L_38 = V_1;
		G_B8_0 = L_37;
		if ((!(((float)L_38) == ((float)(0.0f)))))
		{
			G_B9_0 = L_37;
			goto IL_00f8;
		}
	}
	{
		G_B10_0 = (0.0f);
		G_B10_1 = G_B8_0;
		goto IL_00fb;
	}

IL_00f8:
	{
		float L_39 = V_2;
		float L_40 = V_1;
		G_B10_0 = ((float)((float)L_39/(float)L_40));
		G_B10_1 = G_B9_0;
	}

IL_00fb:
	{
		*((float*)(G_B10_1)) = (float)G_B10_0;
		float* L_41 = ___v3;
		float L_42 = V_1;
		*((float*)(L_41)) = (float)L_42;
		return;
	}
}
// System.Boolean CUIColorPicker::GetLocalMouse(UnityEngine.GameObject,UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR bool CUIColorPicker_GetLocalMouse_m3410064497 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, Vector2_t2156229523 * ___result1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CUIColorPicker_GetLocalMouse_m3410064497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t2360479859  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t2360479859  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Rect_t2360479859  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2156229523  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Rect_t2360479859  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		GameObject_t1113636619 * L_0 = ___go0;
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3704657025 *)CastclassSealed((RuntimeObject*)L_1, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		RectTransform_t3704657025 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_4 = Transform_InverseTransformPoint_m1343916000(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Vector2_t2156229523 * L_5 = ___result1;
		float L_6 = (&V_1)->get_x_2();
		RectTransform_t3704657025 * L_7 = V_0;
		NullCheck(L_7);
		Rect_t2360479859  L_8 = RectTransform_get_rect_m574169965(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector2_t2156229523  L_9 = Rect_get_min_m847841034((Rect_t2360479859 *)(&V_2), /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = (&V_3)->get_x_0();
		RectTransform_t3704657025 * L_11 = V_0;
		NullCheck(L_11);
		Rect_t2360479859  L_12 = RectTransform_get_rect_m574169965(L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		Vector2_t2156229523  L_13 = Rect_get_max_m350137314((Rect_t2360479859 *)(&V_4), /*hidden argument*/NULL);
		V_5 = L_13;
		float L_14 = (&V_5)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_6, L_10, L_14, /*hidden argument*/NULL);
		L_5->set_x_0(L_15);
		Vector2_t2156229523 * L_16 = ___result1;
		float L_17 = (&V_1)->get_y_3();
		RectTransform_t3704657025 * L_18 = V_0;
		NullCheck(L_18);
		Rect_t2360479859  L_19 = RectTransform_get_rect_m574169965(L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		Vector2_t2156229523  L_20 = Rect_get_min_m847841034((Rect_t2360479859 *)(&V_6), /*hidden argument*/NULL);
		V_7 = L_20;
		float L_21 = (&V_7)->get_y_1();
		RectTransform_t3704657025 * L_22 = V_0;
		NullCheck(L_22);
		Rect_t2360479859  L_23 = RectTransform_get_rect_m574169965(L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		Vector2_t2156229523  L_24 = Rect_get_max_m350137314((Rect_t2360479859 *)(&V_8), /*hidden argument*/NULL);
		V_9 = L_24;
		float L_25 = (&V_9)->get_y_1();
		float L_26 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_17, L_21, L_25, /*hidden argument*/NULL);
		L_16->set_y_1(L_26);
		RectTransform_t3704657025 * L_27 = V_0;
		NullCheck(L_27);
		Rect_t2360479859  L_28 = RectTransform_get_rect_m574169965(L_27, /*hidden argument*/NULL);
		V_10 = L_28;
		Vector3_t3722313464  L_29 = V_1;
		bool L_30 = Rect_Contains_m1232228501((Rect_t2360479859 *)(&V_10), L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// UnityEngine.Vector2 CUIColorPicker::GetWidgetSize(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  CUIColorPicker_GetWidgetSize_m2427733028 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CUIColorPicker_GetWidgetSize_m2427733028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1113636619 * L_0 = ___go0;
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3704657025 *)CastclassSealed((RuntimeObject*)L_1, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		RectTransform_t3704657025 * L_2 = V_0;
		NullCheck(L_2);
		Rect_t2360479859  L_3 = RectTransform_get_rect_m574169965(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector2_t2156229523  L_4 = Rect_get_size_m477575021((Rect_t2360479859 *)(&V_1), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.GameObject CUIColorPicker::GO(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * CUIColorPicker_GO_m3538182937 (CUIColorPicker_t3452025155 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		Transform_t3600365921 * L_2 = Transform_Find_m1729760951(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void CUIColorPicker::Setup(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_Setup_m1889472872 (CUIColorPicker_t3452025155 * __this, Color_t2555686324  ___inputColor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CUIColorPicker_Setup_m1889472872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSetupU3Ec__AnonStorey0_t1509386468 * V_0 = NULL;
	Texture2D_t3840446185 * V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_0 = (U3CSetupU3Ec__AnonStorey0_t1509386468 *)il2cpp_codegen_object_new(U3CSetupU3Ec__AnonStorey0_t1509386468_il2cpp_TypeInfo_var);
		U3CSetupU3Ec__AnonStorey0__ctor_m2873649314(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_19(__this);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_2 = V_0;
		GameObject_t1113636619 * L_3 = CUIColorPicker_GO_m3538182937(__this, _stringLiteral3504223596, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_satvalGO_10(L_3);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_4 = V_0;
		GameObject_t1113636619 * L_5 = CUIColorPicker_GO_m3538182937(__this, _stringLiteral72992802, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_satvalKnob_18(L_5);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_6 = V_0;
		GameObject_t1113636619 * L_7 = CUIColorPicker_GO_m3538182937(__this, _stringLiteral1700643193, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_hueGO_8(L_7);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_8 = V_0;
		GameObject_t1113636619 * L_9 = CUIColorPicker_GO_m3538182937(__this, _stringLiteral866954529, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_hueKnob_15(L_9);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_10 = V_0;
		GameObject_t1113636619 * L_11 = CUIColorPicker_GO_m3538182937(__this, _stringLiteral405360011, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_result_7(L_11);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_12 = V_0;
		ColorU5BU5D_t941916413* L_13 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)6);
		ColorU5BU5D_t941916413* L_14 = L_13;
		NullCheck(L_14);
		Color_t2555686324  L_15 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_15;
		ColorU5BU5D_t941916413* L_16 = L_14;
		NullCheck(L_16);
		Color_t2555686324  L_17 = Color_get_yellow_m1287957903(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_17;
		ColorU5BU5D_t941916413* L_18 = L_16;
		NullCheck(L_18);
		Color_t2555686324  L_19 = Color_get_green_m490390750(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_19;
		ColorU5BU5D_t941916413* L_20 = L_18;
		NullCheck(L_20);
		Color_t2555686324  L_21 = Color_get_cyan_m765383084(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_21;
		ColorU5BU5D_t941916413* L_22 = L_20;
		NullCheck(L_22);
		Color_t2555686324  L_23 = Color_get_blue_m3190273327(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(4))) = L_23;
		ColorU5BU5D_t941916413* L_24 = L_22;
		NullCheck(L_24);
		Color_t2555686324  L_25 = Color_get_magenta_m208363462(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(5))) = L_25;
		NullCheck(L_12);
		L_12->set_hueColors_3(L_24);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_26 = V_0;
		ColorU5BU5D_t941916413* L_27 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)4);
		ColorU5BU5D_t941916413* L_28 = L_27;
		NullCheck(L_28);
		Color_t2555686324  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Color__ctor_m286683560((&L_29), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_29;
		ColorU5BU5D_t941916413* L_30 = L_28;
		NullCheck(L_30);
		Color_t2555686324  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Color__ctor_m286683560((&L_31), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_31;
		ColorU5BU5D_t941916413* L_32 = L_30;
		NullCheck(L_32);
		Color_t2555686324  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Color__ctor_m286683560((&L_33), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		*(Color_t2555686324 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_33;
		ColorU5BU5D_t941916413* L_34 = L_32;
		NullCheck(L_34);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_35 = V_0;
		NullCheck(L_35);
		ColorU5BU5D_t941916413* L_36 = L_35->get_hueColors_3();
		NullCheck(L_36);
		*(Color_t2555686324 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = (*(Color_t2555686324 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		NullCheck(L_26);
		L_26->set_satvalColors_1(L_34);
		Texture2D_t3840446185 * L_37 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m373113269(L_37, 1, 7, /*hidden argument*/NULL);
		V_1 = L_37;
		V_2 = 0;
		goto IL_018b;
	}

IL_016c:
	{
		Texture2D_t3840446185 * L_38 = V_1;
		int32_t L_39 = V_2;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_40 = V_0;
		NullCheck(L_40);
		ColorU5BU5D_t941916413* L_41 = L_40->get_hueColors_3();
		int32_t L_42 = V_2;
		NullCheck(L_41);
		NullCheck(L_38);
		Texture2D_SetPixel_m2984741184(L_38, 0, L_39, (*(Color_t2555686324 *)((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_42%(int32_t)6)))))), /*hidden argument*/NULL);
		int32_t L_43 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
	}

IL_018b:
	{
		int32_t L_44 = V_2;
		if ((((int32_t)L_44) < ((int32_t)7)))
		{
			goto IL_016c;
		}
	}
	{
		Texture2D_t3840446185 * L_45 = V_1;
		NullCheck(L_45);
		Texture2D_Apply_m2271746283(L_45, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_46 = V_0;
		NullCheck(L_46);
		GameObject_t1113636619 * L_47 = L_46->get_hueGO_8();
		NullCheck(L_47);
		Image_t2670269651 * L_48 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_47, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		Texture2D_t3840446185 * L_49 = V_1;
		Rect_t2360479859  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Rect__ctor_m2614021312((&L_50), (0.0f), (0.5f), (1.0f), (6.0f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector2__ctor_m3970636864((&L_51), (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_t280657092 * L_52 = Sprite_Create_m803022218(NULL /*static, unused*/, L_49, L_50, L_51, /*hidden argument*/NULL);
		NullCheck(L_48);
		Image_set_sprite_m2369174689(L_48, L_52, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_53 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_54 = V_0;
		NullCheck(L_54);
		GameObject_t1113636619 * L_55 = L_54->get_hueGO_8();
		Vector2_t2156229523  L_56 = CUIColorPicker_GetWidgetSize_m2427733028(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		L_53->set_hueSz_12(L_56);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_57 = V_0;
		Texture2D_t3840446185 * L_58 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m373113269(L_58, 2, 2, /*hidden argument*/NULL);
		NullCheck(L_57);
		L_57->set_satvalTex_0(L_58);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_59 = V_0;
		NullCheck(L_59);
		GameObject_t1113636619 * L_60 = L_59->get_satvalGO_10();
		NullCheck(L_60);
		Image_t2670269651 * L_61 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_60, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_62 = V_0;
		NullCheck(L_62);
		Texture2D_t3840446185 * L_63 = L_62->get_satvalTex_0();
		Rect_t2360479859  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Rect__ctor_m2614021312((&L_64), (0.5f), (0.5f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Vector2__ctor_m3970636864((&L_65), (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_t280657092 * L_66 = Sprite_Create_m803022218(NULL /*static, unused*/, L_63, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_61);
		Image_set_sprite_m2369174689(L_61, L_66, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_67 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_68 = V_0;
		intptr_t L_69 = (intptr_t)U3CSetupU3Ec__AnonStorey0_U3CU3Em__0_m1864855913_RuntimeMethod_var;
		Action_t1264377477 * L_70 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_70, L_68, L_69, /*hidden argument*/NULL);
		NullCheck(L_67);
		L_67->set_resetSatValTexture_4(L_70);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_71 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_72 = V_0;
		NullCheck(L_72);
		GameObject_t1113636619 * L_73 = L_72->get_satvalGO_10();
		Vector2_t2156229523  L_74 = CUIColorPicker_GetWidgetSize_m2427733028(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		NullCheck(L_71);
		L_71->set_satvalSz_17(L_74);
		Color_t2555686324  L_75 = ___inputColor0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_76 = V_0;
		NullCheck(L_76);
		float* L_77 = L_76->get_address_of_Hue_2();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_78 = V_0;
		NullCheck(L_78);
		float* L_79 = L_78->get_address_of_Saturation_5();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_80 = V_0;
		NullCheck(L_80);
		float* L_81 = L_80->get_address_of_Value_6();
		CUIColorPicker_RGBToHSV_m132274398(NULL /*static, unused*/, L_75, (float*)L_77, (float*)L_79, (float*)L_81, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_82 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_83 = V_0;
		intptr_t L_84 = (intptr_t)U3CSetupU3Ec__AnonStorey0_U3CU3Em__1_m3821171049_RuntimeMethod_var;
		Action_t1264377477 * L_85 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_85, L_83, L_84, /*hidden argument*/NULL);
		NullCheck(L_82);
		L_82->set_applyHue_13(L_85);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_86 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_87 = V_0;
		intptr_t L_88 = (intptr_t)U3CSetupU3Ec__AnonStorey0_U3CU3Em__2_m2247192937_RuntimeMethod_var;
		Action_t1264377477 * L_89 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_89, L_87, L_88, /*hidden argument*/NULL);
		NullCheck(L_86);
		L_86->set_applySaturationValue_14(L_89);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_90 = V_0;
		NullCheck(L_90);
		Action_t1264377477 * L_91 = L_90->get_applyHue_13();
		NullCheck(L_91);
		Action_Invoke_m937035532(L_91, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_92 = V_0;
		NullCheck(L_92);
		Action_t1264377477 * L_93 = L_92->get_applySaturationValue_14();
		NullCheck(L_93);
		Action_Invoke_m937035532(L_93, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_94 = V_0;
		NullCheck(L_94);
		GameObject_t1113636619 * L_95 = L_94->get_satvalKnob_18();
		NullCheck(L_95);
		Transform_t3600365921 * L_96 = GameObject_get_transform_m1369836730(L_95, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_97 = V_0;
		NullCheck(L_97);
		float L_98 = L_97->get_Saturation_5();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_99 = V_0;
		NullCheck(L_99);
		Vector2_t2156229523 * L_100 = L_99->get_address_of_satvalSz_17();
		float L_101 = L_100->get_x_0();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_102 = V_0;
		NullCheck(L_102);
		float L_103 = L_102->get_Value_6();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_104 = V_0;
		NullCheck(L_104);
		Vector2_t2156229523 * L_105 = L_104->get_address_of_satvalSz_17();
		float L_106 = L_105->get_y_1();
		Vector2_t2156229523  L_107;
		memset(&L_107, 0, sizeof(L_107));
		Vector2__ctor_m3970636864((&L_107), ((float)il2cpp_codegen_multiply((float)L_98, (float)L_101)), ((float)il2cpp_codegen_multiply((float)L_103, (float)L_106)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_108 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_107, /*hidden argument*/NULL);
		NullCheck(L_96);
		Transform_set_localPosition_m4128471975(L_96, L_108, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_109 = V_0;
		NullCheck(L_109);
		GameObject_t1113636619 * L_110 = L_109->get_hueKnob_15();
		NullCheck(L_110);
		Transform_t3600365921 * L_111 = GameObject_get_transform_m1369836730(L_110, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_112 = V_0;
		NullCheck(L_112);
		GameObject_t1113636619 * L_113 = L_112->get_hueKnob_15();
		NullCheck(L_113);
		Transform_t3600365921 * L_114 = GameObject_get_transform_m1369836730(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		Vector3_t3722313464  L_115 = Transform_get_localPosition_m4234289348(L_114, /*hidden argument*/NULL);
		V_3 = L_115;
		float L_116 = (&V_3)->get_x_2();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_117 = V_0;
		NullCheck(L_117);
		float L_118 = L_117->get_Hue_2();
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_119 = V_0;
		NullCheck(L_119);
		Vector2_t2156229523 * L_120 = L_119->get_address_of_satvalSz_17();
		float L_121 = L_120->get_y_1();
		Vector2_t2156229523  L_122;
		memset(&L_122, 0, sizeof(L_122));
		Vector2__ctor_m3970636864((&L_122), L_116, ((float)il2cpp_codegen_multiply((float)((float)((float)L_118/(float)(6.0f))), (float)L_121)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_123 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
		NullCheck(L_111);
		Transform_set_localPosition_m4128471975(L_111, L_123, /*hidden argument*/NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_124 = V_0;
		NullCheck(L_124);
		L_124->set_dragH_9((Action_t1264377477 *)NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_125 = V_0;
		NullCheck(L_125);
		L_125->set_dragSV_11((Action_t1264377477 *)NULL);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_126 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_127 = V_0;
		intptr_t L_128 = (intptr_t)U3CSetupU3Ec__AnonStorey0_U3CU3Em__3_m4203508073_RuntimeMethod_var;
		Action_t1264377477 * L_129 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_129, L_127, L_128, /*hidden argument*/NULL);
		NullCheck(L_126);
		L_126->set_idle_16(L_129);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_130 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_131 = V_0;
		intptr_t L_132 = (intptr_t)U3CSetupU3Ec__AnonStorey0_U3CU3Em__4_m1100181865_RuntimeMethod_var;
		Action_t1264377477 * L_133 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_133, L_131, L_132, /*hidden argument*/NULL);
		NullCheck(L_130);
		L_130->set_dragH_9(L_133);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_134 = V_0;
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_135 = V_0;
		intptr_t L_136 = (intptr_t)U3CSetupU3Ec__AnonStorey0_U3CU3Em__5_m3056497001_RuntimeMethod_var;
		Action_t1264377477 * L_137 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_137, L_135, L_136, /*hidden argument*/NULL);
		NullCheck(L_134);
		L_134->set_dragSV_11(L_137);
		U3CSetupU3Ec__AnonStorey0_t1509386468 * L_138 = V_0;
		NullCheck(L_138);
		Action_t1264377477 * L_139 = L_138->get_idle_16();
		__this->set__update_6(L_139);
		return;
	}
}
// System.Void CUIColorPicker::SetRandomColor()
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_SetRandomColor_m2343968930 (CUIColorPicker_t3452025155 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CUIColorPicker_SetRandomColor_m2343968930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Random_t108471755 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Random_t108471755 * L_0 = (Random_t108471755 *)il2cpp_codegen_object_new(Random_t108471755_il2cpp_TypeInfo_var);
		Random__ctor_m4122933043(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Random_t108471755 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Random::Next() */, L_1);
		V_1 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_2%(int32_t)((int32_t)1000))))))/(float)(1000.0f)));
		Random_t108471755 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Random::Next() */, L_3);
		V_2 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_4%(int32_t)((int32_t)1000))))))/(float)(1000.0f)));
		Random_t108471755 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Random::Next() */, L_5);
		V_3 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_6%(int32_t)((int32_t)1000))))))/(float)(1000.0f)));
		float L_7 = V_1;
		float L_8 = V_2;
		float L_9 = V_3;
		Color_t2555686324  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m286683560((&L_10), L_7, L_8, L_9, /*hidden argument*/NULL);
		CUIColorPicker_set_Color_m30073458(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CUIColorPicker::Awake()
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_Awake_m1150610550 (CUIColorPicker_t3452025155 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		CUIColorPicker_set_Color_m30073458(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CUIColorPicker::Update()
extern "C" IL2CPP_METHOD_ATTR void CUIColorPicker_Update_m4130195695 (CUIColorPicker_t3452025155 * __this, const RuntimeMethod* method)
{
	{
		Action_t1264377477 * L_0 = __this->get__update_6();
		NullCheck(L_0);
		Action_Invoke_m937035532(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0__ctor_m2873649314 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::<>m__0()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0_U3CU3Em__0_m1864855913 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0034;
	}

IL_000e:
	{
		Texture2D_t3840446185 * L_0 = __this->get_satvalTex_0();
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		ColorU5BU5D_t941916413* L_3 = __this->get_satvalColors_1();
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		NullCheck(L_3);
		NullCheck(L_0);
		Texture2D_SetPixel_m2984741184(L_0, L_1, L_2, (*(Color_t2555686324 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)2)))))))), /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0034:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		Texture2D_t3840446185 * L_10 = __this->get_satvalTex_0();
		NullCheck(L_10);
		Texture2D_Apply_m2271746283(L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::<>m__1()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0_U3CU3Em__1_m3821171049 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupU3Ec__AnonStorey0_U3CU3Em__1_m3821171049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Color_t2555686324  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = __this->get_Hue_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, (((int32_t)((int32_t)L_0))), 0, 5, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1))%(int32_t)6));
		ColorU5BU5D_t941916413* L_3 = __this->get_hueColors_3();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		ColorU5BU5D_t941916413* L_5 = __this->get_hueColors_3();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		float L_7 = __this->get_Hue_2();
		int32_t L_8 = V_0;
		Color_t2555686324  L_9 = Color_Lerp_m973389909(NULL /*static, unused*/, (*(Color_t2555686324 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))), (*(Color_t2555686324 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), ((float)il2cpp_codegen_subtract((float)L_7, (float)(((float)((float)L_8))))), /*hidden argument*/NULL);
		V_2 = L_9;
		ColorU5BU5D_t941916413* L_10 = __this->get_satvalColors_1();
		NullCheck(L_10);
		Color_t2555686324  L_11 = V_2;
		*(Color_t2555686324 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_11;
		Action_t1264377477 * L_12 = __this->get_resetSatValTexture_4();
		NullCheck(L_12);
		Action_Invoke_m937035532(L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::<>m__2()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0_U3CU3Em__2_m2247192937 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupU3Ec__AnonStorey0_U3CU3Em__2_m2247192937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2555686324  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t2555686324  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t2555686324  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t2555686324  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t2555686324  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Image_t2670269651 * V_7 = NULL;
	{
		float L_0 = __this->get_Saturation_5();
		float L_1 = __this->get_Value_6();
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_0), L_0, L_1, /*hidden argument*/NULL);
		float L_2 = (&V_0)->get_x_0();
		float L_3 = (&V_0)->get_y_1();
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_1), ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_2)), ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_3)), /*hidden argument*/NULL);
		float L_4 = (&V_1)->get_x_0();
		float L_5 = (&V_1)->get_y_1();
		ColorU5BU5D_t941916413* L_6 = __this->get_satvalColors_1();
		NullCheck(L_6);
		Color_t2555686324  L_7 = Color_op_Multiply_m2768890077(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), (*(Color_t2555686324 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_0)->get_x_0();
		float L_9 = (&V_1)->get_y_1();
		ColorU5BU5D_t941916413* L_10 = __this->get_satvalColors_1();
		NullCheck(L_10);
		Color_t2555686324  L_11 = Color_op_Multiply_m2768890077(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (*(Color_t2555686324 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_1)->get_x_0();
		float L_13 = (&V_0)->get_y_1();
		ColorU5BU5D_t941916413* L_14 = __this->get_satvalColors_1();
		NullCheck(L_14);
		Color_t2555686324  L_15 = Color_op_Multiply_m2768890077(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)), (*(Color_t2555686324 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = (&V_0)->get_x_0();
		float L_17 = (&V_0)->get_y_1();
		ColorU5BU5D_t941916413* L_18 = __this->get_satvalColors_1();
		NullCheck(L_18);
		Color_t2555686324  L_19 = Color_op_Multiply_m2768890077(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)), (*(Color_t2555686324 *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))), /*hidden argument*/NULL);
		V_5 = L_19;
		Color_t2555686324  L_20 = V_2;
		Color_t2555686324  L_21 = V_3;
		Color_t2555686324  L_22 = Color_op_Addition_m3293657895(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Color_t2555686324  L_23 = V_4;
		Color_t2555686324  L_24 = Color_op_Addition_m3293657895(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Color_t2555686324  L_25 = V_5;
		Color_t2555686324  L_26 = Color_op_Addition_m3293657895(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		GameObject_t1113636619 * L_27 = __this->get_result_7();
		NullCheck(L_27);
		Image_t2670269651 * L_28 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_27, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		V_7 = L_28;
		Image_t2670269651 * L_29 = V_7;
		Color_t2555686324  L_30 = V_6;
		NullCheck(L_29);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_29, L_30);
		CUIColorPicker_t3452025155 * L_31 = __this->get_U24this_19();
		NullCheck(L_31);
		Color_t2555686324  L_32 = L_31->get__color_4();
		Color_t2555686324  L_33 = V_6;
		bool L_34 = Color_op_Inequality_m3353772181(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0141;
		}
	}
	{
		CUIColorPicker_t3452025155 * L_35 = __this->get_U24this_19();
		NullCheck(L_35);
		Action_1_t2728153919 * L_36 = L_35->get__onValueChange_5();
		if (!L_36)
		{
			goto IL_0134;
		}
	}
	{
		CUIColorPicker_t3452025155 * L_37 = __this->get_U24this_19();
		NullCheck(L_37);
		Action_1_t2728153919 * L_38 = L_37->get__onValueChange_5();
		Color_t2555686324  L_39 = V_6;
		NullCheck(L_38);
		Action_1_Invoke_m3561268814(L_38, L_39, /*hidden argument*/Action_1_Invoke_m3561268814_RuntimeMethod_var);
	}

IL_0134:
	{
		CUIColorPicker_t3452025155 * L_40 = __this->get_U24this_19();
		Color_t2555686324  L_41 = V_6;
		NullCheck(L_40);
		L_40->set__color_4(L_41);
	}

IL_0141:
	{
		return;
	}
}
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::<>m__3()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0_U3CU3Em__3_m4203508073 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupU3Ec__AnonStorey0_U3CU3Em__3_m4203508073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t1113636619 * L_1 = __this->get_hueGO_8();
		bool L_2 = CUIColorPicker_GetLocalMouse_m3410064497(NULL /*static, unused*/, L_1, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		CUIColorPicker_t3452025155 * L_3 = __this->get_U24this_19();
		Action_t1264377477 * L_4 = __this->get_dragH_9();
		NullCheck(L_3);
		L_3->set__update_6(L_4);
		goto IL_0056;
	}

IL_0033:
	{
		GameObject_t1113636619 * L_5 = __this->get_satvalGO_10();
		bool L_6 = CUIColorPicker_GetLocalMouse_m3410064497(NULL /*static, unused*/, L_5, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		CUIColorPicker_t3452025155 * L_7 = __this->get_U24this_19();
		Action_t1264377477 * L_8 = __this->get_dragSV_11();
		NullCheck(L_7);
		L_7->set__update_6(L_8);
	}

IL_0056:
	{
		return;
	}
}
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::<>m__4()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0_U3CU3Em__4_m1100181865 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupU3Ec__AnonStorey0_U3CU3Em__4_m1100181865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1113636619 * L_0 = __this->get_hueGO_8();
		CUIColorPicker_GetLocalMouse_m3410064497(NULL /*static, unused*/, L_0, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		float L_1 = (&V_0)->get_y_1();
		Vector2_t2156229523 * L_2 = __this->get_address_of_hueSz_12();
		float L_3 = L_2->get_y_1();
		__this->set_Hue_2(((float)il2cpp_codegen_multiply((float)((float)((float)L_1/(float)L_3)), (float)(6.0f))));
		Action_t1264377477 * L_4 = __this->get_applyHue_13();
		NullCheck(L_4);
		Action_Invoke_m937035532(L_4, /*hidden argument*/NULL);
		Action_t1264377477 * L_5 = __this->get_applySaturationValue_14();
		NullCheck(L_5);
		Action_Invoke_m937035532(L_5, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_hueKnob_15();
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_hueKnob_15();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_localPosition_m4234289348(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_2();
		float L_12 = (&V_0)->get_y_1();
		Vector2_t2156229523  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector2__ctor_m3970636864((&L_13), L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_14 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localPosition_m4128471975(L_7, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0098;
		}
	}
	{
		CUIColorPicker_t3452025155 * L_16 = __this->get_U24this_19();
		Action_t1264377477 * L_17 = __this->get_idle_16();
		NullCheck(L_16);
		L_16->set__update_6(L_17);
	}

IL_0098:
	{
		return;
	}
}
// System.Void CUIColorPicker/<Setup>c__AnonStorey0::<>m__5()
extern "C" IL2CPP_METHOD_ATTR void U3CSetupU3Ec__AnonStorey0_U3CU3Em__5_m3056497001 (U3CSetupU3Ec__AnonStorey0_t1509386468 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSetupU3Ec__AnonStorey0_U3CU3Em__5_m3056497001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1113636619 * L_0 = __this->get_satvalGO_10();
		CUIColorPicker_GetLocalMouse_m3410064497(NULL /*static, unused*/, L_0, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		float L_1 = (&V_0)->get_x_0();
		Vector2_t2156229523 * L_2 = __this->get_address_of_satvalSz_17();
		float L_3 = L_2->get_x_0();
		__this->set_Saturation_5(((float)((float)L_1/(float)L_3)));
		float L_4 = (&V_0)->get_y_1();
		Vector2_t2156229523 * L_5 = __this->get_address_of_satvalSz_17();
		float L_6 = L_5->get_y_1();
		__this->set_Value_6(((float)((float)L_4/(float)L_6)));
		Action_t1264377477 * L_7 = __this->get_applySaturationValue_14();
		NullCheck(L_7);
		Action_Invoke_m937035532(L_7, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_satvalKnob_18();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localPosition_m4128471975(L_9, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		CUIColorPicker_t3452025155 * L_13 = __this->get_U24this_19();
		Action_t1264377477 * L_14 = __this->get_idle_16();
		NullCheck(L_13);
		L_13->set__update_6(L_14);
	}

IL_007d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Painter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Painter__ctor_m2888580210 (Painter_t2372114961 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Painter::Awake()
extern "C" IL2CPP_METHOD_ATTR void Painter_Awake_m3430070722 (Painter_t2372114961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_Awake_m3430070722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__resetColour_9(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Painter_t2372114961_il2cpp_TypeInfo_var);
		((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->set__drawable_6(__this);
		intptr_t L_1 = (intptr_t)Painter_PenBrush_m323187877_RuntimeMethod_var;
		BrushFunction_t136629315 * L_2 = (BrushFunction_t136629315 *)il2cpp_codegen_object_new(BrushFunction_t136629315_il2cpp_TypeInfo_var);
		BrushFunction__ctor_m2232455995(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set__currentBrush_8(L_2);
		SpriteRenderer_t3235626157 * L_3 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		NullCheck(L_3);
		Sprite_t280657092 * L_4 = SpriteRenderer_get_sprite_m663386871(L_3, /*hidden argument*/NULL);
		__this->set__drawableSprite_12(L_4);
		Sprite_t280657092 * L_5 = __this->get__drawableSprite_12();
		NullCheck(L_5);
		Texture2D_t3840446185 * L_6 = Sprite_get_texture_m3976398399(L_5, /*hidden argument*/NULL);
		__this->set__drawableTexture_13(L_6);
		Painter_ResetCanvas_m1980404891(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Painter::Update()
extern "C" IL2CPP_METHOD_ATTR void Painter_Update_m1027624347 (Painter_t2372114961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_Update_m1027624347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2D_t2806799626 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m513753021(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0070;
		}
	}
	{
		Camera_t4157153871 * L_1 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_3 = Camera_ScreenToWorldPoint_m3978588570(L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t2156229523  L_5 = V_0;
		LayerMask_t3493934918 * L_6 = __this->get_address_of__drawingLayers_7();
		int32_t L_7 = LayerMask_get_value_m1881709263((LayerMask_t3493934918 *)L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2D_t2806799626 * L_8 = Physics2D_OverlapPoint_m3522226740(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Collider2D_t2806799626 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		Collider2D_t2806799626 * L_11 = V_1;
		NullCheck(L_11);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_12, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0060;
		}
	}
	{
		BrushFunction_t136629315 * L_14 = __this->get__currentBrush_8();
		Vector2_t2156229523  L_15 = V_0;
		NullCheck(L_14);
		BrushFunction_Invoke_m4261340326(L_14, L_15, /*hidden argument*/NULL);
		goto IL_006b;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_16 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__previousDragPosition_14(L_16);
	}

IL_006b:
	{
		goto IL_0086;
	}

IL_0070:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_17 = Input_GetMouseButton_m513753021(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0086;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_18 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__previousDragPosition_14(L_18);
	}

IL_0086:
	{
		return;
	}
}
// System.Void Painter::ColourPixels(UnityEngine.Vector2,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_ColourPixels_m1360549721 (Painter_t2372114961 * __this, Vector2_t2156229523  ___centerPixel0, int32_t ___penThickness1, Color_t2555686324  ___colorPen2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		float L_0 = (&___centerPixel0)->get_x_0();
		V_0 = (((int32_t)((int32_t)L_0)));
		float L_1 = (&___centerPixel0)->get_y_1();
		V_1 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_0;
		int32_t L_3 = ___penThickness1;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3));
		goto IL_0043;
	}

IL_001b:
	{
		int32_t L_4 = V_1;
		int32_t L_5 = ___penThickness1;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5));
		goto IL_0036;
	}

IL_0024:
	{
		Texture2D_t3840446185 * L_6 = __this->get__drawableTexture_13();
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		Color_t2555686324  L_9 = ___colorPen2;
		NullCheck(L_6);
		Texture2D_SetPixel_m2984741184(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0036:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_1;
		int32_t L_13 = ___penThickness1;
		if ((((int32_t)L_11) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_13)))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		int32_t L_17 = ___penThickness1;
		if ((((int32_t)L_15) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17)))))
		{
			goto IL_001b;
		}
	}
	{
		Texture2D_t3840446185 * L_18 = __this->get__drawableTexture_13();
		NullCheck(L_18);
		Texture2D_Apply_m2271746283(L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Painter::PenBrush(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Painter_PenBrush_m323187877 (Painter_t2372114961 * __this, Vector2_t2156229523  ___worldPoint0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_PenBrush_m323187877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = ___worldPoint0;
		Vector2_t2156229523  L_1 = Painter_WorldToPixelCoordinates_m3555177886(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Texture2D_t3840446185 * L_2 = __this->get__drawableTexture_13();
		NullCheck(L_2);
		Color32U5BU5D_t3850468773* L_3 = Texture2D_GetPixels32_m647746242(L_2, /*hidden argument*/NULL);
		__this->set__curentColors_11(L_3);
		Vector2_t2156229523  L_4 = __this->get__previousDragPosition_14();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_6 = Vector2_op_Equality_m2303255133(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		Vector2_t2156229523  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Painter_t2372114961_il2cpp_TypeInfo_var);
		int32_t L_8 = ((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->get_PenWidth_5();
		Color_t2555686324  L_9 = ((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->get_PenColour_4();
		Painter_MarkPixelsToColour_m2454846674(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0044:
	{
		Vector2_t2156229523  L_10 = __this->get__previousDragPosition_14();
		Vector2_t2156229523  L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Painter_t2372114961_il2cpp_TypeInfo_var);
		int32_t L_12 = ((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->get_PenWidth_5();
		Color_t2555686324  L_13 = ((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->get_PenColour_4();
		Painter_Interpolation_m2325092361(__this, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
	}

IL_005b:
	{
		Painter_ApplyMarkedPixelChanges_m581631983(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = V_0;
		__this->set__previousDragPosition_14(L_14);
		return;
	}
}
// System.Void Painter::MarkPixelsToColour(UnityEngine.Vector2,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_MarkPixelsToColour_m2454846674 (Painter_t2372114961 * __this, Vector2_t2156229523  ____centerPixel0, int32_t ___penThickness1, Color_t2555686324  ____penColor2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Rect_t2360479859  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	{
		float L_0 = (&____centerPixel0)->get_x_0();
		V_0 = (((int32_t)((int32_t)L_0)));
		float L_1 = (&____centerPixel0)->get_y_1();
		V_1 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_0;
		int32_t L_3 = ___penThickness1;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3));
		goto IL_0069;
	}

IL_001b:
	{
		int32_t L_4 = V_2;
		Sprite_t280657092 * L_5 = __this->get__drawableSprite_12();
		NullCheck(L_5);
		Rect_t2360479859  L_6 = Sprite_get_rect_m2575211689(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = Rect_get_width_m3421484486((Rect_t2360479859 *)(&V_3), /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)(((int32_t)((int32_t)L_7))))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0041;
		}
	}

IL_003c:
	{
		goto IL_0065;
	}

IL_0041:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___penThickness1;
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)L_10));
		goto IL_005b;
	}

IL_004b:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_4;
		Color_t2555686324  L_13 = ____penColor2;
		Painter_MarkPixelToChange_m2658964883(__this, L_11, L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_15 = V_4;
		int32_t L_16 = V_1;
		int32_t L_17 = ___penThickness1;
		if ((((int32_t)L_15) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17)))))
		{
			goto IL_004b;
		}
	}

IL_0065:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0069:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = V_0;
		int32_t L_21 = ___penThickness1;
		if ((((int32_t)L_19) <= ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)L_21)))))
		{
			goto IL_001b;
		}
	}
	{
		return;
	}
}
// System.Void Painter::MarkPixelToChange(System.Int32,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_MarkPixelToChange_m2658964883 (Painter_t2372114961 * __this, int32_t ___x0, int32_t ___y1, Color_t2555686324  ____color2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___y1;
		Sprite_t280657092 * L_1 = __this->get__drawableSprite_12();
		NullCheck(L_1);
		Rect_t2360479859  L_2 = Sprite_get_rect_m2575211689(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_width_m3421484486((Rect_t2360479859 *)(&V_1), /*hidden argument*/NULL);
		int32_t L_4 = ___x0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)(((int32_t)((int32_t)L_3))))), (int32_t)L_4));
		int32_t L_5 = V_0;
		Color32U5BU5D_t3850468773* L_6 = __this->get__curentColors_11();
		NullCheck(L_6);
		if ((((int32_t)L_5) > ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length)))))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}

IL_002e:
	{
		return;
	}

IL_002f:
	{
		Color32U5BU5D_t3850468773* L_8 = __this->get__curentColors_11();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Color_t2555686324  L_10 = ____color2;
		Color32_t2600501292  L_11 = Color32_op_Implicit_m2658259763(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		*(Color32_t2600501292 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))) = L_11;
		return;
	}
}
// System.Void Painter::ApplyMarkedPixelChanges()
extern "C" IL2CPP_METHOD_ATTR void Painter_ApplyMarkedPixelChanges_m581631983 (Painter_t2372114961 * __this, const RuntimeMethod* method)
{
	{
		Texture2D_t3840446185 * L_0 = __this->get__drawableTexture_13();
		Color32U5BU5D_t3850468773* L_1 = __this->get__curentColors_11();
		NullCheck(L_0);
		Texture2D_SetPixels32_m1141065075(L_0, L_1, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_2 = __this->get__drawableTexture_13();
		NullCheck(L_2);
		Texture2D_Apply_m2271746283(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Painter::Interpolation(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Painter_Interpolation_m2325092361 (Painter_t2372114961 * __this, Vector2_t2156229523  ____startPoint0, Vector2_t2156229523  ____endPoint1, int32_t ____width2, Color_t2555686324  ____color3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_Interpolation_m2325092361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		Vector2_t2156229523  L_0 = ____startPoint0;
		Vector2_t2156229523  L_1 = ____endPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		float L_2 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_t2156229523  L_3 = ____startPoint0;
		Vector2_t2156229523  L_4 = ____endPoint1;
		Vector2_t2156229523  L_5 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t2156229523  L_6 = Vector2_get_normalized_m2683665860((Vector2_t2156229523 *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		Vector2_t2156229523  L_7 = ____startPoint0;
		V_3 = L_7;
		float L_8 = V_0;
		V_4 = ((float)((float)(1.0f)/(float)L_8));
		V_5 = (0.0f);
		goto IL_004a;
	}

IL_002f:
	{
		Vector2_t2156229523  L_9 = ____startPoint0;
		Vector2_t2156229523  L_10 = ____endPoint1;
		float L_11 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_12 = Vector2_Lerp_m854472224(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector2_t2156229523  L_13 = V_3;
		int32_t L_14 = ____width2;
		Color_t2555686324  L_15 = ____color3;
		Painter_MarkPixelsToColour_m2454846674(__this, L_13, L_14, L_15, /*hidden argument*/NULL);
		float L_16 = V_5;
		float L_17 = V_4;
		V_5 = ((float)il2cpp_codegen_add((float)L_16, (float)L_17));
	}

IL_004a:
	{
		float L_18 = V_5;
		if ((((float)L_18) <= ((float)(1.0f))))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector2 Painter::WorldToPixelCoordinates(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Painter_WorldToPixelCoordinates_m3555177886 (Painter_t2372114961 * __this, Vector2_t2156229523  ____worldPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_WorldToPixelCoordinates_m3555177886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Rect_t2360479859  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Bounds_t2266837910  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector2_t2156229523  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_1 = ____worldPosition0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_3 = Transform_InverseTransformPoint_m1343916000(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Sprite_t280657092 * L_4 = __this->get__drawableSprite_12();
		NullCheck(L_4);
		Rect_t2360479859  L_5 = Sprite_get_rect_m2575211689(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_width_m3421484486((Rect_t2360479859 *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		Sprite_t280657092 * L_7 = __this->get__drawableSprite_12();
		NullCheck(L_7);
		Rect_t2360479859  L_8 = Sprite_get_rect_m2575211689(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_height_m1358425599((Rect_t2360479859 *)(&V_4), /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = V_1;
		Sprite_t280657092 * L_11 = __this->get__drawableSprite_12();
		NullCheck(L_11);
		Bounds_t2266837910  L_12 = Sprite_get_bounds_m2438297458(L_11, /*hidden argument*/NULL);
		V_6 = L_12;
		Vector3_t3722313464  L_13 = Bounds_get_size_m1178783246((Bounds_t2266837910 *)(&V_6), /*hidden argument*/NULL);
		V_7 = L_13;
		float L_14 = (&V_7)->get_x_2();
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_localScale_m129152068(L_15, /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = (&V_8)->get_x_2();
		V_5 = ((float)il2cpp_codegen_multiply((float)((float)((float)L_10/(float)L_14)), (float)L_17));
		float L_18 = (&V_0)->get_x_2();
		float L_19 = V_5;
		float L_20 = V_1;
		V_9 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_18, (float)L_19)), (float)((float)((float)L_20/(float)(2.0f)))));
		float L_21 = (&V_0)->get_y_3();
		float L_22 = V_5;
		float L_23 = V_3;
		V_10 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_21, (float)L_22)), (float)((float)((float)L_23/(float)(2.0f)))));
		float L_24 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_25 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		float L_26 = V_10;
		int32_t L_27 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_11), (((float)((float)L_25))), (((float)((float)L_27))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_28 = V_11;
		return L_28;
	}
}
// System.Void Painter::ResetCanvas()
extern "C" IL2CPP_METHOD_ATTR void Painter_ResetCanvas_m1980404891 (Painter_t2372114961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_ResetCanvas_m1980404891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Sprite_t280657092 * L_0 = __this->get__drawableSprite_12();
		NullCheck(L_0);
		Rect_t2360479859  L_1 = Sprite_get_rect_m2575211689(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_width_m3421484486((Rect_t2360479859 *)(&V_0), /*hidden argument*/NULL);
		Sprite_t280657092 * L_3 = __this->get__drawableSprite_12();
		NullCheck(L_3);
		Rect_t2360479859  L_4 = Sprite_get_rect_m2575211689(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = Rect_get_height_m1358425599((Rect_t2360479859 *)(&V_1), /*hidden argument*/NULL);
		ColorU5BU5D_t941916413* L_6 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)(((int32_t)((int32_t)L_2))), (int32_t)(((int32_t)((int32_t)L_5))))));
		__this->set__cleanColoursArray_10(L_6);
		V_2 = 0;
		goto IL_0056;
	}

IL_003b:
	{
		ColorU5BU5D_t941916413* L_7 = __this->get__cleanColoursArray_10();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Color_t2555686324  L_9 = __this->get__resetColour_9();
		*(Color_t2555686324 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0056:
	{
		int32_t L_11 = V_2;
		ColorU5BU5D_t941916413* L_12 = __this->get__cleanColoursArray_10();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_003b;
		}
	}
	{
		Texture2D_t3840446185 * L_13 = __this->get__drawableTexture_13();
		ColorU5BU5D_t941916413* L_14 = __this->get__cleanColoursArray_10();
		NullCheck(L_13);
		Texture2D_SetPixels_m3008871897(L_13, L_14, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_15 = __this->get__drawableTexture_13();
		NullCheck(L_15);
		Texture2D_Apply_m2271746283(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Painter::SetPenBrush()
extern "C" IL2CPP_METHOD_ATTR void Painter_SetPenBrush_m424290498 (Painter_t2372114961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter_SetPenBrush_m424290498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)Painter_PenBrush_m323187877_RuntimeMethod_var;
		BrushFunction_t136629315 * L_1 = (BrushFunction_t136629315 *)il2cpp_codegen_object_new(BrushFunction_t136629315_il2cpp_TypeInfo_var);
		BrushFunction__ctor_m2232455995(L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set__currentBrush_8(L_1);
		return;
	}
}
// System.Void Painter::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Painter__cctor_m662125785 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Painter__cctor_m662125785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->set_PenColour_4(L_0);
		((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->set_PenWidth_5(3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_BrushFunction_t136629315 (BrushFunction_t136629315 * __this, Vector2_t2156229523  ___world_position0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(Vector2_t2156229523 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___world_position0);

}
// System.Void Painter/BrushFunction::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void BrushFunction__ctor_m2232455995 (BrushFunction_t136629315 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Painter/BrushFunction::Invoke(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void BrushFunction_Invoke_m4261340326 (BrushFunction_t136629315 * __this, Vector2_t2156229523  ___world_position0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		BrushFunction_Invoke_m4261340326((BrushFunction_t136629315 *)__this->get_prev_9(), ___world_position0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, Vector2_t2156229523 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___world_position0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, Vector2_t2156229523 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___world_position0, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Vector2_t2156229523  >::Invoke(targetMethod, targetThis, ___world_position0);
					else
						GenericVirtActionInvoker1< Vector2_t2156229523  >::Invoke(targetMethod, targetThis, ___world_position0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Vector2_t2156229523  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___world_position0);
					else
						VirtActionInvoker1< Vector2_t2156229523  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___world_position0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Vector2_t2156229523 , const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___world_position0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Painter/BrushFunction::BeginInvoke(UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* BrushFunction_BeginInvoke_m3305966453 (BrushFunction_t136629315 * __this, Vector2_t2156229523  ___world_position0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BrushFunction_BeginInvoke_m3305966453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &___world_position0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Painter/BrushFunction::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void BrushFunction_EndInvoke_m642429080 (BrushFunction_t136629315 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PaintingSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings__ctor_m1840393661 (PaintingSettings_t3763250382 * __this, const RuntimeMethod* method)
{
	{
		__this->set__transparency_4((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PaintingSettings::Start()
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_Start_m1354421605 (PaintingSettings_t3763250382 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PaintingSettings_Start_m1354421605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		CUIColorPicker_t3452025155 * L_0 = Object_FindObjectOfType_TisCUIColorPicker_t3452025155_m1733115665(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisCUIColorPicker_t3452025155_m1733115665_RuntimeMethod_var);
		__this->set__cUIColorPicker_5(L_0);
		return;
	}
}
// System.Void PaintingSettings::SetMarkerColour(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetMarkerColour_m484989563 (PaintingSettings_t3763250382 * __this, Color_t2555686324  ___new_color0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PaintingSettings_SetMarkerColour_m484989563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = ___new_color0;
		IL2CPP_RUNTIME_CLASS_INIT(Painter_t2372114961_il2cpp_TypeInfo_var);
		((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->set_PenColour_4(L_0);
		return;
	}
}
// System.Void PaintingSettings::SetMarkerWidth(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetMarkerWidth_m3501128933 (PaintingSettings_t3763250382 * __this, int32_t ___new_width0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PaintingSettings_SetMarkerWidth_m3501128933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___new_width0;
		IL2CPP_RUNTIME_CLASS_INIT(Painter_t2372114961_il2cpp_TypeInfo_var);
		((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->set_PenWidth_5(L_0);
		return;
	}
}
// System.Void PaintingSettings::SetMarkerWidth(System.Single)
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetMarkerWidth_m3559340404 (PaintingSettings_t3763250382 * __this, float ___new_width0, const RuntimeMethod* method)
{
	{
		float L_0 = ___new_width0;
		PaintingSettings_SetMarkerWidth_m3501128933(__this, (((int32_t)((int32_t)L_0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PaintingSettings::SetNewColor()
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetNewColor_m3890990801 (PaintingSettings_t3763250382 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PaintingSettings_SetNewColor_m3890990801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CUIColorPicker_t3452025155 * L_0 = __this->get__cUIColorPicker_5();
		NullCheck(L_0);
		Color_t2555686324  L_1 = CUIColorPicker_get_Color_m3988971789(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = __this->get__transparency_4();
		(&V_0)->set_a_3(L_2);
		Color_t2555686324  L_3 = V_0;
		PaintingSettings_SetMarkerColour_m484989563(__this, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Painter_t2372114961_il2cpp_TypeInfo_var);
		Painter_t2372114961 * L_4 = ((Painter_t2372114961_StaticFields*)il2cpp_codegen_static_fields_for(Painter_t2372114961_il2cpp_TypeInfo_var))->get__drawable_6();
		NullCheck(L_4);
		Painter_SetPenBrush_m424290498(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PaintingSettings::SetEraser()
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_SetEraser_m1706179406 (PaintingSettings_t3763250382 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2943235014((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		PaintingSettings_SetMarkerColour_m484989563(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PaintingSettings::PartialSetEraser()
extern "C" IL2CPP_METHOD_ATTR void PaintingSettings_PartialSetEraser_m127484490 (PaintingSettings_t3763250382 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2943235014((&L_0), (255.0f), (255.0f), (255.0f), (0.5f), /*hidden argument*/NULL);
		PaintingSettings_SetMarkerColour_m484989563(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SaveHealper::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SaveHealper__ctor_m4090097154 (SaveHealper_t3871815534 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SaveHealper::Save(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void SaveHealper_Save_m4095981465 (SaveHealper_t3871815534 * __this, Texture2D_t3840446185 * ___texture2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SaveHealper_Save_m4095981465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	DateTime_t3738529785  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Texture2D_t3840446185 * L_0 = ___texture2D0;
		ByteU5BU5D_t4116647657* L_1 = ImageConversion_EncodeToPNG_m2292631531(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Application_get_dataPath_m4232621142(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, L_2, _stringLiteral3684129841, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		bool L_5 = Directory_Exists_m1484791558(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_6 = V_1;
		Directory_CreateDirectory_m751642867(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_7 = DateTime_get_Now_m1277138875(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_7;
		String_t* L_8 = DateTime_ToString_m3718521780((DateTime_t3738529785 *)(&V_3), _stringLiteral2671228154, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = V_1;
		String_t* L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2163913788(NULL /*static, unused*/, L_9, _stringLiteral819375327, L_10, _stringLiteral2410385622, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_12 = V_0;
		File_WriteAllBytes_m4252682105(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
